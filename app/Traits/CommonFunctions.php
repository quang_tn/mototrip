<?php

namespace App\Traits;

use Cviebrock\EloquentSluggable\Sluggable;
use Carbon\Carbon;

trait CommonFunctions
{
    use Sluggable;

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }
    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function otherComments()
    {
        return $this->morphMany(OtherComment::class, 'commentable')->latest();
    }

    public function confirmedBookings()
    {
        return $this->morphMany(ConfirmedBooking::class, 'bookable');
    }

    public function options()
    {
        return $this->morphMany(Option::class, 'optionable');
    }

    public function comments()
    {
        return $this->morphMany('App\Comment', 'commentable')->whereNull('parent_comment_id')->latest();
    }

    public function couponSchedules()
    {
        return $this->morphMany('App\CouponSchedule', 'couponable');
    }

    /* Surcharge Dates */
    public function surchargeDates()
    {
        return $this->morphToMany('App\SurchargeDate', 'surchargeable');//->first();
    }
    /* Return 'Tour' or 'Transfer' */
    public function baseClassName()
    {
        return (new \ReflectionClass($this))->getShortName();
    }

    //Get price for closest date
    public function getPriceNowAttribute()
    {
        $key = $this->baseClassName() . '_' . $this->id . '_priceNow';
        if (!cache()->has($key)) {
            $closestDate = array_first($this->scheduledDates());
            cache()->forever($key, isset($closestDate['coupon']) ? $this->scheduledFees()[$closestDate['scheduleId']][1][1] * (1 - $closestDate['coupon'][array_key_first($closestDate['coupon'])] / 100)
                : $this->scheduledFees()[$closestDate['scheduleId']][1][1]);
        }
        return cache($key);
    }
    public function getPriceForDate($date)
    {
        $selectedDate = $this->scheduledDates()[$date] ?? null;
        $price = null;
        if($selectedDate) {
            $price = isset($selectedDate['coupon']) ? $this->scheduledFees()[$selectedDate['scheduleId']][1][1] * (1 - $selectedDate['coupon'][array_key_first($selectedDate['coupon'])] / 100)
                : $this->scheduledFees()[$selectedDate['scheduleId']][1][1];
        }
        return $price;
    }
    public function getScheduledDatesAttribute()
    {
        return $this->scheduledDates();
    }
    public function scheduledDates() //Only used for display all dates with available options on Availability component
    {
        $key = $this->baseClassName() . '_' . $this->id . '_scheduledDates';
        if (!cache()->has($key)) {
            $scheduledDates = [];
            $couponData = [];
            //$scheduledFees = $this->scheduledFees();
            //$priceNow = 0;
            //Set coupon data by dates
            if ($this->couponSchedules->count()) { //dd('aa');
                foreach ($this->couponSchedules as $couponSchedule) {
                    if (now('Asia/Ho_Chi_Minh')->startOfDay()->between(Carbon::parse($couponSchedule->booking_start,'Asia/Ho_Chi_Minh'), Carbon::parse($couponSchedule->booking_end,'Asia/Ho_Chi_Minh'))) {

                        $scheduleStart = Carbon::parse($couponSchedule->travel_start);
                        $scheduleEnd = Carbon::parse($couponSchedule->travel_end);
                        $rule = (new \Recurr\Rule)->setStartDate($this->defineStartDate($scheduleStart))->setFreq('DAILY')->setUntil(Carbon::parse($scheduleEnd));
                        $transform = (new \Recurr\Transformer\ArrayTransformer())->transform($rule);
                        foreach ($transform as $day) {
                            $date = $day->getStart()->format('Y-m-d');
                            foreach ($couponSchedule->coupons->sortBy('travelerGroup.name') as $coupon) {
                                $couponData[$date][$coupon->travelerGroup->slug] = $coupon->discount_percent;
                            }
                        }
                    } else {
                        $couponSchedule->delete();
                    }
                }
            }
            //dd($this->couponSchedules->first());
            //Set price data by dates
            foreach ($this->options as $option) {
                foreach ($option->schedules as $schedule) {
                    //Build array of prices tier based on number of pax
                    /* foreach ($schedule->prices as $j=>$price) {
                    $scheduledFees[$schedule->id][$j+1] =
                            call_user_func(function() use ($price) {
                                $a=['group'=>$price->travelerGroup];
                                $lastUpper = 1;
                                foreach(get_object_vars(json_decode($price->tier)) as $upperRange=>$fee) {
                                    for ($i=$lastUpper; $i <= $upperRange; $i++) {
                                        $a[$i]=$fee;
                                    }
                                    $lastUpper=$upperRange+1;
                                }
                                return $a;
                            });
                } */
                    //$scheduledFees[$schedule->id] = $fees;

                    $scheduleStart = Carbon::parse($schedule->start_date);
                    $scheduleEnd = Carbon::parse($schedule->good_till);
                    $rule = (new \Recurr\Rule)->setStartDate($this->defineStartDate($scheduleStart))->setFreq($schedule->repeat == 1 ? 'DAILY' : 'WEEKLY')->setUntil($scheduleEnd);
                    //dd($scheduleStart);
                    if ($schedule->repeat == 2) $rule = $rule->setByDay(json_decode($schedule->on));
                    $transform = (new \Recurr\Transformer\ArrayTransformer())->transform($rule);
                    foreach ($transform as $day) {

                        $date = $day->getStart()->format('Y-m-d'); //dd($date);
                        if (!in_array($date, json_decode($schedule->not_on)) && !isset($scheduledDates[$date])) {
                            $scheduledDates[$date]['scheduleId'] = $schedule->id;
                            if (isset($couponData[$date])) {
                                $scheduledDates[$date]['coupon'] = $couponData[$date];
                                //Calculate price if booking made now
                                //if($priceNow==0) $priceNow = $scheduledFees[$scheduledDates[$date]['scheduleId']][1][1] * (1-$couponData[$date][array_key_first($couponData[$date])]/100);
                            } /* else {
                            if($priceNow==0) $priceNow = $scheduledFees[$scheduledDates[$date]['scheduleId']][1][1];
                        } */

                            //Set surcharge dates
                            if (in_array($date, json_decode($this->surchargeDates()->first()->on ?? '[]'))) $scheduledDates[$date]['surcharge'] = ['surcharge' => $this->surchargeDates()->first()->surcharge, 'type' => $this->surchargeDates()->first()->type];
                        }
                    }
                }
            }
            uksort($scheduledDates, 'order_date');
            cache()->forever($key, $scheduledDates);



            /* cache()->forever('tour_'.$this->id.'_priceNow',$priceNow); */
        }
        return cache($key);
    }
    public function defineStartDate($pricePeriodStart=null)
    {
        if($this->baseClassName()=='Transfer') {
            $sameDateBookable=false;
            for($h=now('Asia/Ho_Chi_Minh')->hour+1;$h<24;$h++) {
                $cutoff=$this->cutoff;
                if($h>=10 && $h<=22) $cutoff=5;
                $nextValidTime = now('Asia/Ho_Chi_Minh')->addHours($cutoff);
                if($nextValidTime->isBefore(now('Asia/Ho_Chi_Minh')->setHour($h)->startOfHour())) {$sameDateBookable=true; break;}
            }
            if(!$sameDateBookable) $nextValidTime = now('Asia/Ho_Chi_Minh')->addDay();
        }
        /* if ($nextValidTime->hour > explode(':', $this->departure_time)[0]) $validStartDate = $nextValidTime->addDay();
        else $validStartDate = $nextValidTime; */
        if($this->baseClassName()=='Tour') {
            $nextValidTime=now('Asia/Ho_Chi_Minh')->addHours($this->cutoff);
            if($nextValidTime->hour > explode(':', $this->departure_time)[0]) $nextValidTime = $nextValidTime->addDay();
        }

        if (($pricePeriodStart ?? now('Asia/Ho_Chi_Minh'))->gt($nextValidTime)) $nextValidTime = $pricePeriodStart;
        //dd($validStartDate);
        return $nextValidTime->startOfDay();
    }
    public function removeInvalidDatesRunHourly()
    {
        $validStart = $this->defineStartDate();
        foreach($dates = $this->scheduledDates() as $date=>$data) {
            $dateParse = Carbon::parse($date,'Asia/Ho_Chi_Minh');
            if($dateParse->gte($validStart)) break 1;
            else {
                unset($dates[$date]);
                //Remove possible expired couponSchedule
                if ($this->couponSchedules->count()) {
                    if($dateParse->lte(Carbon::parse($this->couponSchedules->first()->booking_end,'Asia/Ho_Chi_Minh'))) $this->couponSchedules->first()->delete();
                }
            }
        }
        cache()->forever($this->baseClassName() . '_' . $this->id . '_scheduledDates',$dates);
    }

    public function scheduledFees()
    {
        $key = $this->baseClassName() . '_' . $this->id . '_scheduledFees';
        if (!cache()->has($key)) {
            $scheduledFees = [];
            foreach ($this->options as $option) {
                foreach ($option->schedules as $schedule) {
                    //Build array of prices tier based on number of pax
                    $j=0;
                    foreach ($schedule->prices->sortBy('travelerGroup.name') as $price) {
                        $j++;
                        $scheduledFees[$schedule->id][$j] =
                            call_user_func(function () use ($price) {
                                $a = ['group' => $price->travelerGroup];
                                $lastUpper = 1;
                                $tier = get_object_vars(json_decode($price->tier));
                                ksort($tier);
                                foreach ($tier as $upperRange => $fee) {
                                    for ($i = $lastUpper; $i <= $upperRange; $i++) {
                                        $a[$i] = $fee;
                                    }
                                    $lastUpper = $upperRange + 1;
                                }
                                return $a;
                            });
                    }
                }
            }
            cache()->forever($key, $scheduledFees);
        }
        return cache($key);
    }

    public function getAverageRatingAttribute()
    {
        return $this->average_rating();
    }
    public function average_rating()
    {
        $key = $this->baseClassName() . '_' . $this->id . '_averageRating';
        if (!cache()->has($key)) {
            $total_rating = 0;
            $parent_comments = 0;
            $total_other_rating = 0;
            if ($this->comments->count()) {
                foreach ($this->comments as $comment) {
                    $total_rating += $comment->rating;
                    $parent_comments++;
                }
                $total_rating = $total_rating / $parent_comments;
            }
            $parent_comments = 0;
            if ($this->otherComments->count()) {
                foreach ($this->otherComments as $comment) {
                    $total_other_rating += $comment->rating;
                    $parent_comments++;
                }
                $total_other_rating = $total_other_rating / $parent_comments;
            }
            cache()->forever($key, [
                'averageRating' => round($total_rating, 0),
                'averageOtherRating' => round($total_other_rating, 0)
            ]);
        }
        return cache($key);
    }
}
