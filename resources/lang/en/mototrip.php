<?php

return [
    'adults' => 'Adult|Adults|{0} Adult',
    'children' => 'Child|Children|{0} Child',
    'infants' => 'Infant|Infants|{0} Infant',
    'day' => 'day|days',
    'review' => 'review|reviews',
    'person' => 'Person|Persons'
];
