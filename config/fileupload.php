<?php

return [
    UPLOAD_AVATAR_IMAGE => [
        'path_upload' => 'avatar_image/%s',
    ],
    UPLOAD_COVER_IMAGE => [
        'path_upload' => 'cover_image/%s',
    ],
    UPLOAD_VIDEO_TYPE => [
        'paths' => [
            'path_upload' => 'videos/%s/%s',
            'path_origin' => 'videos/%s/%s/origin',
            'path_flavor' => 'videos/%s/%s/flavor',
            'path_trim' => 'videos/%s/%s/cut',
            'path_convert' => 'converts/%s',
        ],
        'mime_type' => [
            'video/x-flv',
            'video/mp4',
            'video/mpv'
        ],
        'extension' => [
            'flv', 'f4v', 'f4p', 'f4a', 'f4b',
            'mp4', 'mpv', 'mpg4', 'mp4v'
        ],
        'time_video_url_expire' => env('DOCUMENT_URL_EXPIRE') ?: '1 minute',
        'time_thumbnail_url_expire' => env('THUMBNAIL_URL_EXPIRE') ?: '1 day',
        'disk' => [
            'download_local' => 'public',
            'convert' => 'public',
            'upload' => 's3',
        ],
    ],
    'avatar_lives' => [
        'disk' => 's3',
        'path_upload' => 'uploads/images/lives/avatars/%s',
        'avatar_url_expire' => env('LIVE_AVATAR_URL_EXPIRE', '1 day'),
        'thumbnail_width' => env('LIVE_THUMBNAIL_WIDTH', 320),
    ],
    'avatar_users' => [
        'disk' => 's3',
        'path_upload' => 'uploads/images/users/avatars/%s',
        'avatar_url_expire' => env('USER_AVATAR_URL_EXPIRE', '1 day'),
        'thumbnail_width' => env('USER_THUMBNAIL_WIDTH', 320),
    ],
];
