<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function switchCurrency()
    {
        session(['currency' => request('currency')]);
        return redirect('/');
    }
}
