<?php

namespace Tests\Feature;

use App\Services\UploadMedia\Helpers\ImageHelper;
use App\SocialIdentity;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Storage;
use Tests\Mocking\SocialiteMock;
use Tests\TestCase;

class SocialLoginTest extends TestCase
{
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();

        $this->withExceptionHandling();
    }

    public function testSocialLoginNewUser()
    {
        session()->put('provider', 'google');
        $abstractUser = SocialiteMock::abstractUser();
        SocialiteMock::fake($abstractUser);
        /*$this->mock(ImageHelper::class, function ($mock) {
            $mock->shouldReceive('upload')->once()->andReturn('images/avatars/123.jpg');
        });*/

        $this->get('/callback');
        // $this->assertEquals('images/avatars/123.jpg', User::latest()->first()->avatar);
        $this->assertDatabaseCount('users',1)->assertDatabaseCount('social_identities',1)
            ->assertTrue(auth()->check());

    }

    public function testSocialLoginWithExistedUser()
    {
        $user = factory(User::class)->create();
        $socialId = rand();
        SocialIdentity::create([
            'user_id' => $user->id,
            'provider' => 'facebook',
            'provider_user_id' => $socialId
        ]);
        $abstractUser = SocialiteMock::abstractUser(['email' => $user->email]);
        SocialiteMock::fake($abstractUser);
        session()->put('provider', 'google');
        $this->get('/callback');
        $this->assertDatabaseCount('users', 1)->assertDatabaseCount('social_identities', 2)
            ->assertTrue(auth()->check());
    }
}
