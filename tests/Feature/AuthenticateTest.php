<?php

namespace Tests\Feature;

use App\Notifications\VerifyEmail;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class AuthenticateTest extends TestCase
{
    use RefreshDatabase;

    private $user;
    private $socialUser;
    private $file;

    public function setUp(): void
    {
        parent::setUp();

        //$this->withoutExceptionHandling();
        $this->user = factory(User::class)->create(['password' => bcrypt('Password!123')]);
        Storage::fake('avatars');
        $this->file = UploadedFile::fake()->image('avatar.jpg');
    }

    public function testCanRegisterNewUser()
    {
        $user = factory(User::class)->make();
        Notification::fake();
        $params = [
            'title' => $user->title,
            'firstname' => $user->firstname,
            'lastname' => $user->lastname,
            'avatar' => $this->file,
            'email' => $user->email,
            'password' => '1a23B456!78'
        ];
        Notification::assertNothingSent();
        $response = $this->postJson('/register', $params);

        $response->assertStatus(Response::HTTP_CREATED);
        $response->assertJsonStructure([
            'loggedIn',
            'user' => ['id', 'title', 'firstname', 'lastname', 'email',
            ]
        ]);

        $this->assertDatabaseHas('users', [
            'firstname' => $this->user->firstname,
            'lastname' => $this->user->lastname,
            'email' => $this->user->email
        ]);

        $body = json_decode($response->getContent(), true);

        $this->assertTrue($body['loggedIn']);

        //Check Email Verification Notification sent
        Notification::assertSentTo(
            [auth()->user()], VerifyEmail::class
        );

        //Check user can verify his email
        $response = $this->get($this->verificationUrl(auth()->user()));
        $response->assertStatus(\Illuminate\Http\Response::HTTP_FOUND);
        $this->assertNotNull(auth()->user()->email_verified_at);
    }

    public function testUserCanLoginWithCorrectInformation()
    {
        $params = [
            'email' => $this->user->email,
            'password' => 'Password!123',
        ];

        $response = $this->postJson('/login', $params);

        $response->assertStatus(Response::HTTP_OK);
        $response->assertJsonStructure([
            'loggedIn',
            'user' => [
                'id', 'email'
            ]
        ]);
    }

    public function testUserCantLoginWithWrongInformation()
    {
        $params = [
            'email' => $this->user->email,
            'password' => 'wrong-password'
        ];

        $response = $this->postJson('/login', $params);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function testUserCanLogout()
    {
        $this->actingAs($this->user);

        $response = $this->post('/logout');

        $response->assertRedirect('/');
    }

    protected function verificationUrl($notifiable)
    {
        return URL::temporarySignedRoute(
            'verification.verify',
            Carbon::now()->addMinutes(Config::get('auth.verification.expire', 60)),
            [
                'id' => $notifiable->getKey(),
                'hash' => sha1($notifiable->getEmailForVerification()),
            ]
        );
        /* $hash = Crypt::encrypt($notifiable->getKey());

        return config('frontend.email_verify_url') . $hash; */
    }
}
