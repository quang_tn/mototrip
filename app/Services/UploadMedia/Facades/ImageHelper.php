<?php
namespace App\Services\UploadMedia\Facades;

use Illuminate\Support\Facades\Facade;

class ImageHelper extends Facade
{
    public static function getFacadeAccessor()
    {
        return \App\Services\UploadMedia\Helpers\ImageHelper::class;
    }
}
