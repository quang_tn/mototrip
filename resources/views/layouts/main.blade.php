<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible"
          content="IE=edge">
    <meta name="viewport"
          content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    <!-- META DATA -->
    <meta name="description"
          content="Indochina Unique Tourist a tours supplier acrossing Viet Nam, Laos, ThaiLand, Cambodia, Myanmar..."/>
    <meta name="keywords"
          content="Indochine Tourist, SouthEast Asia Tours, Tours to Viet Nam, Bicycle Tours in Hoi An, Basket Boat Tour, Hue City Tour, My Son Tour, Da Nang"/>
    <meta name="author"
          content="Indochina Unique Tourist">
    <!-- CSRF Token -->
    <meta name="csrf-token"
          content="{{ csrf_token() }}">
    <title>@yield('title')</title>
    <link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap"
          rel="stylesheet">
    <!-- CUSTOM CSS -->
{{-- <link href="{{asset('css/main_layout.css?v=5')}}" rel="stylesheet"> --}}
<!-- FAVICON -->
    <link rel="shortcut icon"
          href="/favicon.ico">
    <link rel="apple-touch-icon"
          href="/images/apple-touch-icon.png?v=2">
    <link rel="apple-touch-icon"
          sizes="72x72"
          href="/images/apple-touch-icon-72x72.png?v=2">
    <link rel="apple-touch-icon"
          sizes="114x114"
          href="/images/apple-touch-icon-114x114.png?v=2">
    @yield('header')
    <style>
        .cls-1 {
            fill: #ffc200
        }

        #main-content {
            padding-top: 57px;
        }

        .modal::-webkit-scrollbar {
            display: none;
        }

        /* Hide scrollbar for IE, Edge and Firefox */
        .modal {
            -ms-overflow-style: none; /* IE and Edge */
            scrollbar-width: none; /* Firefox */
        }

        @media (max-width: 640px) {
            nav#main-nav .active, nav#user-nav .active {
                display: block !important;
            }

            nav#main-nav .dropdown {
                top: 57px;
            }

            nav#user-nav .dropdown {
                top: 50px;
            }

            .toasted-container .toasted {
                padding-top: 4px !important;
                padding-bottom: 4px !important;
                justify-content: center !important;
            }
        }

        .toasted > svg:first-child {
            margin-top: -0.1rem;
            margin-right: 0.3rem;
        }

        .toasted.bubble .action {
            color: yellow !important;
        }

        @media (min-width: 768px) {

        }
    </style>
    <link rel="stylesheet"
          href="/css/app.css">

    <script src="https://kit.fontawesome.com/655fea1972.js"
            crossorigin="anonymous"></script>
</head>


<body class="font-sans antialiased overflow-x-hidden"
      style="max-width: 100vw;left:0;right:0;">
<div class="main-wrapper"
     id="app">
    {{-- <signup
            :data={{json_encode(['showSignup'=>session('showSignup'), 'providerUserID'=>session('providerUserID')])}}>
    </signup>
    --}}
    @if(session()->has('message'))
        <toast-alert
            message="{{session('message')}}" type="{{session('type')}}" :action="{{json_encode(session('action'))}}">
        </toast-alert>
    @endif
    @include('partials.navbar')
    <div id="main-content">
        @yield('content')

        @include('partials.footer')
    </div>
    {{-- @include('partials.modals.signup') --}}
    {{-- @guest

    <login :data="{{session()->has('showLogin') ? session('showLogin') : 'null'}}"
    prev-url="{{session('prev_url') ?? ''}}"></login>
    @endguest --}}
    {{-- @include('partials.modals.subscribe_modal') --}}
    {{-- <license-modal></license-modal> --}}
    <login-register :should-show="@json(session()->has('showLogin'))" :show-register="@json(session('register'))"
                    prev-url="{{session('prevUrl') ?? ''}}"></login-register>

    <reset-password :should-show="@json(session('showForgot'))" token="{{session('token')}}"
                    email="{{session('email')}}"></reset-password>
    <!-- Your customer chat code -->
    {{-- <div id="fb-root"></div>
    <div class="fb-customerchat"
         attribution=setup_tool
         page_id="152573868264658"
         logged_in_greeting="Hi! Where do you want to explore next?"
         logged_out_greeting="Welcome to Vietnam Experiences Travel"
         greeting_dialog_display="fade"
         greeting_dialog_delay="10"
         theme_color="#fa3c4c">
    </div> --}}
</div>
<!-- JAVASCRIPTS -->

{{-- @if(!request()->is('/'))
<script src="/plugins/jquery/jquery-2.2.4.min.js"></script>
@else
<script src="/js/two_jquery.js"></script>
@endif --}}
{{-- <script src="/js/bundle_all.js?v=9"></script> --}}
@yield('footer')
@if(!request()->is('admin/*'))
    <script src="{{mix('/js/app.js')}}"></script>
    <script src="//www.google.com/recaptcha/api.js?onload=vueRecaptchaApiLoaded&render=explicit"
            async
            defer></script>
@else
    <script src="{{mix('/js/app_admin.js')}}"></script>
@endif
<script src="https://cdn.jsdelivr.net/ga-lite/latest/ga-lite.min.js"
        async></script>
<script async
        src="https://www.jscache.com/wejs?wtype=rated&amp;uniq=577&amp;locationId=4318205&amp;lang=en_US&amp;display_version=2"
        data-loadtrk
        onload="this.loadtrk=true"></script>
<script>
    var galite = galite || {};
    galite.UA = 'UA-138439658-1'; // Insert your tracking code here
    /* $(function(){
        $(".searchBox").on('show.bs.dropdown', function () {
            setTimeout(function(){
                $('#tourKeyword1').focus()
            },200)
        });
    }) */
    function toggleDropdown() {
        return window.outerWidth > 768 ? true : false
    }
</script>


</body>

</html>
