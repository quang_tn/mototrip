const plugin = require("tailwindcss/plugin");

module.exports = {
    purge: {
        content: ["./src/**/*.html"],

        // These options are passed through directly to PurgeCSS
        options: {
            whitelist: ["bg-red-500", "px-4"]
        }
    },
    theme: {
        extend: {
            colors: {
                orangeMain: "#ffc200"
            },
            height: {
                "1/3": "33%"
            },
            minHeight: {
                "18": "70px"
            },
            zIndex: {
                "100": "100",
                "200": "200"
            }
        }
    },
    variants: {
        display: ["responsive", "important"],
        padding: ["responsive", "important"]
    },
    plugins: [
        plugin(function ({ addVariant }) {
            addVariant("important", ({ container }) => {
                container.walkRules(rule => {
                    rule.selector = `.\\!${rule.selector.slice(1)}`;
                    rule.walkDecls(decl => {
                        decl.important = true;
                    });
                });
            });
        })
    ]
};
