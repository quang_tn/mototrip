export default {
    state: {
        modalRef: '',
        user: {},
        loggedIn: false
    },
    mutations: {
        SET_MODAL_REF(state, ref) {
            state.modalRef = ref
        },
        SET_USER(state, user) {
            state.user = user
        },
        SET_LOGGED_IN(state, loggedIn) {
            state.loggedIn = loggedIn
        }
    }
}