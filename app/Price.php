<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Price extends Model
{
    protected $guarded = [];
    protected $with=['travelerGroup'];

    public function travelerGroup()
    {
        return $this->belongsTo(TravelerGroup::class);
    }
}
