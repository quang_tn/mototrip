<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOtherCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('other_comments', function (Blueprint $table) {
            $table->id();
            $table->string('from');
            $table->string('testimonial_id');
            $table->string('firstname');
            $table->string('avatar')->nullable();
            $table->tinyInteger('rating');
            $table->text('body')->nullable();
            $table->morphs('commentable');
            $table->timestamps();
            $table->unique(['testimonial_id', 'from']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('other_comments');
    }
}
