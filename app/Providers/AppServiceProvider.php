<?php

namespace App\Providers;

use App\Location;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        JsonResource::withoutWrapping();

        if (!cache()->has('locations')) {
            cache()->put('locations', Location::where('status', Location::STATUS['ACTIVE'])
                ->select('id', 'name', 'status')->get());
        }

        View::share('locations', cache('locations'));
    }
}
