<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    /**
     * Don't auto-apply mass assignment protection.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['favoritedModel'];

    /**
     * Fetch an activity feed for the given user.
     *
     * @param User $user
     * @return \Illuminate\Database\Eloquent\Collection;
     */
    public static function feed(User $user)
    {
        return static::where('user_id', $user->id)
            ->latest()
            ->with('subject')
            ->paginate(10);
    }

    /**
     * Fetch the model record for the subject of the favorite.
     */
    public function getFavoritedModelAttribute()
    {
        $favoritedModel = null;

        if ($this->subject_type === Favorite::class) {
            $subject = $this->subject()->firstOrFail();

            if ($subject->favorited_type == Tour::class) {
                $favoritedModel = Tour::find($subject->favorited_id);
            }
        }

        return $favoritedModel;
    }

    /**
     * Fetch the associated subject for the activity.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function subject()
    {
        return $this->morphTo();
    }
}
