@extends('layouts.main')

@section('header')

@section('content')
    @include('partials.user_navbar')
    <div class="container mx-auto">
        <div class="py-3 px-6 text-white grid grid-cols-1 row-gap-3 md:grid-cols-3 md:col-gap-3" style="margin-top: 50px;">
            <div class="w-full rounded bg-teal-400">
                <div class="flex p-4 justify-between items-center h-32">
                    <div>
                        <h1 class="font-bold text-3xl">{{$statistics['trips_count']}}</h1>
                        <p>Trips</p></div>
                    <span class="rounded-full border-white p-1 bg-teal-500 p-4"><i class="fas fa-biking"></i></span>
                </div>
                <a href="/admin/trips">
                    <div class="bg-teal-500 h-12 px-4 flex justify-between items-center">
                        <p>Manage Trips</p>
                        <i class="fas fa-arrow-right"></i>
                    </div>
                </a>
            </div>
            <div class="w-full rounded bg-red-400">
                <div class="flex p-4 justify-between items-center h-32">
                    <div>
                        <h1 class="font-bold text-3xl">{{$statistics['invoices_count']}}</h1>
                        <p>Invoices</p></div>
                    <span class="rounded-full border-white p-1 bg-red-500 p-4"><i
                            class="fas fa-money-bill-alt"></i></span>
                </div>
                <a href="/admin/invoices">
                    <div class="bg-red-500 h-12 px-4 flex justify-between items-center">
                        <p>Manage Invoices</p>
                        <i class="fas fa-arrow-right"></i>
                    </div>
                </a>
            </div>
            <div class="w-full rounded bg-green-400">
                <div class="flex p-4 justify-between items-center h-32">
                    <div>
                        <h1 class="font-bold text-3xl">{{$statistics['users_count']}}</h1>
                        <p>Users</p></div>
                    <span class="rounded-full border-white p-1 bg-green-500 p-4"><i class="fas fa-users"></i></span>
                </div>
                <a href="/admin/users">
                    <div class="bg-green-500 h-12 px-4 flex justify-between items-center">
                        <p>Manage Users</p>
                        <i class="fas fa-users"></i>
                    </div>
                </a>
            </div>
        </div>
    </div>
@endsection
