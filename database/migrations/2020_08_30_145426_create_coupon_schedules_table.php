<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCouponSchedulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coupon_schedules', function (Blueprint $table) {
            $table->id();
            $table->date('booking_start');
            $table->date('booking_end');
            $table->date('travel_start');
            $table->date('travel_end');
            $table->morphs('couponable');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coupon_schedules');
    }
}
