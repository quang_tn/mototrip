<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CouponSchedule extends Model
{
    public $timestamps = false;
    protected $guarded = [];
    protected $with = ['coupons'];

    protected static function boot()
    {
        parent::boot();
        static::deleting(function ($model) {
            $model->coupons->each->delete();
        });
    }

    public function coupons()
    {
        return $this->hasMany('App\Coupon');
    }

    public function couponable()
    {
        return $this->morphTo();
    }
}
