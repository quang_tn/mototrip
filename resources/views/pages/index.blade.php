@extends('layouts.main')
@section('title','Indochina Unique Tourist')
@section('header')
<style>

</style>
@endsection
@section('content')

<slider></slider>

<section class="px-3 py-4 bg-blue-900 text-white md:px-6 md:py-8">
    <div class="container mx-auto md:grid md:grid-cols-12 md:col-gap-2">
        <div class="md:col-span-6 lg:col-span-7">
            <h1 class="text-orangeMain text-3xl font-medium leading-10 md:text-5xl">Perfect vehicles for
                perfect trips
            </h1>
            <h3 class="text-orangeMain text-2xl leading-loose font-medium">Central of Vietnam</h3>
            <p class="my-3 text-xl font-medium text-white tracking-tighter">Welcome to MotoTrip- small agency with a big
                heart
                and passion for
                organising exquisite
                motorcycle tours in Vietnam. We make the plans and do the homework and you get the most beautiful
                two-wheeled
                adventure.</p>
            <a href=""
               class="uppercase mt-3 p-1 font-medium inline-block bg-orangeMain rounded text-sm text-white transform -skew-x-12 hover:bg-yellow-600">
                <span class="transform skew-x-12 block ml-2">Find out more <i
                       class="fas fa-chevron-right ml-2"></i></span></a>
        </div>
        <div class="hidden md:block bg-orangeMain transform -skew-x-12 md:col-span-1 ml-6 mr-8"
             style="height:200px;width:1px;"></div>
        <hr class="mt-4 mb-2 md:hidden">
        <div class="text-xl md:col-span-5 lg:col-span-4">
            <p><i class="
         fas
         fa-chevron-right
         fa-sm
         mr-1
         text-orangeMain"></i> Guided motorcycle tours in Central Vietnam
            </p>
            <p><i class="fas fa-chevron-right fa-sm mr-1 mt-1 text-orangeMain"></i> Triumph motorcycles rental</p>
            <p><i class="fas fa-chevron-right fa-sm mr-1 text-orangeMain"></i> Small groups</p>
            <p><i class="fas fa-chevron-right fa-sm mr-1 text-orangeMain"></i> Friendly atmosphere</p>
            <p><i class="fas fa-chevron-right fa-sm mr-1 text-orangeMain"></i> Exquisite local gastronomy</p>
            <p><i class="fas fa-chevron-right fa-sm mr-1 text-orangeMain"></i> Self-guided packages</p>
        </div>
    </div>
</section>

<section class="bg-white text-white px-3 py-4">
    <div class="container mx-auto md:grid md:grid-cols-12 md:col-gap-4">
        <div class="md:col-span-7">
            <h1 class="text-gray-700 text-3xl font-medium leading-snug">Favorite tours</h1>
            <div class="my-3 grid grid-cols-1 col-gap-4 row-gap-3 lg:grid-cols-2">
                <div class="relative">
                    <div class="absolute top-0 left-0 ml-1">
                        <h3 class="font-medium py-1 px-2 rounded mt-1"
                            style="background:rgba(20,47,58,0.6) url('/images/road-pattern.png') repeat">Bike Tour</h3>
                        <p class="py-1 px-2 rounded mt-1"
                           style="background:rgba(20,47,58,0.6) url('/images/road-pattern.png') repeat">1 day</p>
                    </div>
                    <img src="/images/yxfmm3gmkm.jpg"
                         class="rounded w-full h-auto"
                         alt="">
                    <div class="min-h-18 rounded-bl rounded-br flex justify-between items-center"
                         style="background:#142f3a url('/images/road-pattern.png') repeat-x">
                         <div class=" pl-3 pt-2">
                        <h3 class="font-medium">Danang Food Tour</h3>
                        <p class="text-sm text-orangeMain italic">From $23/pax (min 2 pax)</p>
                        </div>
                        <a href="/trips/danang-food-tour"><button
                                    class="rounded transform -skew-x-12 py-1 px-3 bg-orangeMain mr-4"><i
                                   class="fas fa-chevron-right fa-sm"></i></button></a>
                    </div>
                </div>

                <div class="relative">
                    <div class="absolute top-0 left-0 ml-1">
                        <h3 class="font-medium py-1 px-2 rounded mt-1"
                            style="background:rgba(20,47,58,0.6) url('/images/road-pattern.png') repeat">Car Tour</h3>
                        <p class="py-1 px-2 rounded mt-1"
                           style="background:rgba(20,47,58,0.6) url('/images/road-pattern.png') repeat">5 days</p>
                    </div>
                    <img src="/images/qjhnruc9gr.jpg"
                         class="rounded"
                         alt="">
                    <div class="min-h-18 rounded-bl rounded-br flex justify-between items-center"
                         style="background:#142f3a url('/images/road-pattern.png') repeat-x">
                        <h3 class="font-medium pl-3 pt-2">Hoi An - Danang - Hue DMZ</h3>
                        <button class="rounded transform -skew-x-12 py-1 px-3 bg-orangeMain mr-4"><i
                               class="fas fa-chevron-right fa-sm"></i></button>
                    </div>
                </div>

                <div class="relative">
                    <div class="absolute top-0 left-0 ml-1">
                        <h3 class="font-medium py-1 px-2 rounded mt-1"
                            style="background:rgba(20,47,58,0.6) url('/images/road-pattern.png') repeat">Bike Tour</h3>
                        <p class="py-1 px-2 rounded mt-1"
                           style="background:rgba(20,47,58,0.6) url('/images/road-pattern.png') repeat">1 day</p>
                    </div>
                    <img src="/images/yxfmm3gmkm.jpg"
                         class="rounded w-full h-auto"
                         alt="">
                    <div class="min-h-18 rounded-bl rounded-br flex justify-between items-center"
                         style="background:#142f3a url('/images/road-pattern.png') repeat-x">
                        <h3 class="font-medium pl-3 pt-2">Danang Food Tour</h3>
                        <button class="rounded transform -skew-x-12 py-1 px-3 bg-orangeMain mr-4"><i
                               class="fas fa-chevron-right fa-sm"></i></button>
                    </div>
                </div>

                <div class="relative">
                    <div class="absolute top-0 left-0 ml-1">
                        <h3 class="font-medium py-1 px-2 rounded mt-1"
                            style="background:rgba(20,47,58,0.6) url('/images/road-pattern.png') repeat">Car Tour</h3>
                        <p class="py-1 px-2 rounded mt-1"
                           style="background:rgba(20,47,58,0.6) url('/images/road-pattern.png') repeat">5 days</p>
                    </div>
                    <img src="/images/qjhnruc9gr.jpg"
                         class="rounded"
                         alt="">
                    <div class="min-h-18 rounded-bl rounded-br flex justify-between items-center"
                         style="background:#142f3a url('/images/road-pattern.png') repeat-x">
                        <h3 class="font-medium pl-3 pt-2">Hoi An - Danang - Hue DMZ</h3>
                        <button class="rounded transform -skew-x-12 py-1 px-3 bg-orangeMain mr-4"><i
                               class="fas fa-chevron-right fa-sm"></i></button>
                    </div>
                </div>
            </div>
        </div>
        <div class="md:col-span-5">
            <h1 class="text-gray-700 text-3xl font-medium mb-3">Testimonials</h1>
            <testimonials-slider></testimonials-slider>
        </div>
    </div>
</section>

{{-- <section class="bg-white px-3 py-4 relative">
    
</section> --}}

<section class="bg-gray-300 px-3 py-4 font-medium">
    <div class="container mx-auto md:grid md:grid-cols-2">
        <img src="/images/triumph-bonneville-t100-jet-black.png"
             alt=""
             class="px-3">
        <div class="md:my-auto">
            <h1 class="text-gray-600 text-3xl text-center md:text-left">Our favorite bike</h1>
            <h3 class="text-orangeMain text-2xl mt-5">Have no limits. Ride the best.</h3>
            <p class="text-xl text-blue-900 tracking-tighter">The Bonneville T100 is the one with the classy looks and
                the 60's
                styling adds to its distinctive look. It gets twin peashooter exhausts, two-tone</p>
            <a href="/our-bikes"
               class="uppercase mt-3 p-1 font-medium inline-block bg-orangeMain rounded text-sm text-white transform -skew-x-12 hover:bg-yellow-600">
                <span class="transform skew-x-12 block ml-2">Find out more <i
                       class="fas fa-chevron-right ml-2"></i></span></a>
        </div>
    </div>
</section>

{{-- <search-tours :locations="{{json_encode($locations)}}"
:categories="{{json_encode($categories)}}"></search-tours>
<div style="margin-top:7px"
     v-if="loading">
    <rise-loader :loading="loading"
                 color="#da521e"
                 size="15px"></rise-loader>
</div>
<tours-result></tours-result>
<highlight-tours></highlight-tours>
<div id="testimonials"
     :class="$mq=='xs' || $mq=='sm' ? 'container-fluid' : 'container'"
     style="margin-top:15px">
    <div class="section-heading text-center">

        <h2 class="heading-text"><span style="color:#e42524">Testimonials</span></h2>
        <hr class="lines">
        <p class="sub-heading">
            <span style="color:#00cdaf"><strong>{{substr(cache('total_rank'),0,stripos(cache('total_rank'),' '))}}
                </strong></span>{{substr(cache('total_rank'),stripos(cache('total_rank'),' '))}}
        </p>
        <p class="sub-heading">
            Enjoyed our tours? <a href="https://bit.ly/2AzMbno"
               target="_blank">
                <em>Please share with us</em></a>
        </p>

    </div>
    <testimonials></testimonials>
</div>

<section class="brandSliderSection bg-ash"
         style="background-color: #fffff7;padding:0 0 15px;margin:15px 0 0;">
    <div class="container">
        <div style="text-align:center;margin:10px 0 20px;font-size:20px;font-weight:600;font-style:italic;">Our Travel
            Associations</div>
        <div class="brandSliderr owl-carousel">
            <div class="item text-center">
                <img src="/images/clients/duan_29.png"
                     width="90px"
                     alt="image">
            </div>
            <div class="item text-center">
                <img src="/images/clients/duan_30.png"
                     width="90px"
                     alt="image">
            </div>
            <div class="item text-center">
                <img src="/images/clients/duan_31.png"
                     width="90px"
                     alt="image">
            </div>
        </div>
    </div>
</section> --}}
@endsection

@section('footer')
{{-- <script src="js/homepage_combined.js?v=3"></script> --}}
@endsection