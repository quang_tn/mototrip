@extends('layouts.main')

@section('header')

@section('content')
    @include('partials.user_navbar')
    <div class="container mx-auto px-6 mb-3" style="margin-top: 62px;">
        <albums-listing :data="{{json_encode($albums)}}" :locations="{{json_encode($locations)}}">
        </albums-listing>
    </div>
@endsection
