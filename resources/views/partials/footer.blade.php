<footer class="bg-blue-900 py-4 px-3 md:text-right">
    <a href="/"
       class="text-sm text-orangeMain">Home <span class="text-orangeMain mx-2">|</span> </a>
    <a href="/trips"
       class="text-sm text-white">Trips <span class="text-orangeMain mx-2">|</span> </a>
    <a href="/about-us"
       class="text-sm text-white">About Us <span class="text-orangeMain mx-2">|</span> </a>
    <a href="/contact"
       class="text-sm text-white">Contact <span class="text-orangeMain mx-2">|</span> </a>
    <a href="/terms-conditions"
       class="text-sm text-white">Terms and conditions</a>
    <p class="text-sm text-white mt-2">&copy; {{now()->year}} All copyrights reserved.</p>
</footer>