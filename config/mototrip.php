<?php

return [
    'administrators' => ['it@iut.vn', 'md@iut.vn', 'opt5@iut.vn'],
    'social_login' => [
        'providers' => [
            'facebook',
            'google'
        ]
    ],
    'file_upload' => [
        'disk' => env('FILESYSTEM_DRIVER','public'),
        'avatars' => [
            'path' => 'images/avatars'
        ],
        'comments' => [
            'path' => 'images/comments'
        ],
        'albums' => [
            'path' => 'images/albums'
        ]
    ]
];
