<?php

use App\Location;
use Illuminate\Database\Seeder;

class LocationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $locations = [
            ['Da Nang', 'VietNam'],
            ['Hoi An', 'VietNam'],
            ['Hue', 'VietNam'],
            ['My Son', 'VietNam']
        ];

        foreach ($locations as $location) {
            Location::create([
                'name' => $location[0],
                'country' => $location[1]
            ]);
        }
    }
}
