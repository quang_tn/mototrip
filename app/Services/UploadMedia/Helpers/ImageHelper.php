<?php

namespace App\Services\UploadMedia\Helpers;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class ImageHelper
{

    /**
     * Config image
     *
     * @var array
     */
    protected $config;

    /**
     * Disk
     *
     * @var string
     */
    protected $disk;

    /**
     * Constructor.
     *
     * @return void
     */
    public function __construct()
    {
        $this->config = config('image');

        $this->disk = config('mototrip.file_upload.disk');
    }

    /**
     * Get config file.
     *
     * @param string $option Option
     *
     * @return string|array
     */
    public function getConfig($option = null)
    {
        if ($option === null) {
            return $this->config;
        }

        return array_get($this->config, $option);
    }

    /**
     * Upload file.
     *
     * @param string $path Path
     * @param mixed $file File
     *
     * @return string|array
     */
    public function upload($path, $file)
    {
        $this->createPath($path);

        return Storage::disk($this->getDisk())->putFile($path, $file);
    }

    /**
     * Create path to save document.
     *
     * @param string $paths Path save
     *
     * @return string
     */
    protected function createPath($paths)
    {
        if ($this->getDisk() == 'local') {
            if (!Storage::disk($this->getDisk())->has($paths)) {
                Storage::disk($this->getDisk())->makeDirectory($paths);
            }
        }

        return $paths;
    }

    /**
     * Get disk driver
     *
     * @return $disk
     */
    public function getDisk()
    {
        return $this->disk;
    }

    /**
     * Set disk driver
     *
     * @param string|null $name Disk driver
     *
     * @return $this
     */
    public function setDisk(string $name = null)
    {
        $this->disk = !empty($name) ? $name : config('mototrip.file_upload.disk');

        return $this;
    }

    public function uploadWithFileContent($path, $fileContent)
    {
        $this->createPath($path);

        return Storage::disk($this->getDisk())->put($path, $fileContent);
    }

    /**
     * Get image size
     *
     * @param UploadedFile $file UploadedFile
     *
     * @return array|null
     */
    public function getImageSize(UploadedFile $file)
    {
        try {
            $sizeInfo = getimagesize($file);
            if (count($sizeInfo)) {
                return [
                    'width' => $sizeInfo[0],
                    'height' => $sizeInfo[1]
                ];
            }
        } catch (\Exception $ex) {
            //
        }

        return null;
    }

    /**
     * Delete images.
     *
     * @param string $fileName File Name
     * @param string|null $path Path
     *
     * @return bool
     */
    public function delete($fileName, $path = null)
    {
        $fullPath = is_null($path) ? $fileName : $path . '/' . $fileName;

        return Storage::disk($this->getDisk())->delete($fullPath);
    }

    /**
     * Resize image
     *
     * @param string $path Path
     * @param UploadedFile $file Upload file
     *
     * @return string
     */
    public function uploadThumbnail(string $path, UploadedFile $file, int $width = 320)
    {
        $image = $this->resizeImage($file, $width);
        if (Storage::disk($this->getDisk())->put($path, (string)$image->encode())) {
            return $path;
        }
        return null;
    }

    /**
     * Resize image
     *
     * @param UploadedFile $file UploadedFile
     * @param int $width Width
     *
     * @return \Intervention\Image\Image
     */
    public function resizeImage(UploadedFile $file, int $width = 320)
    {
        $file = $file->openFile();
        $content = $file->fread($file->getSize());
        $image = Image::make($content);

        if ($image->width() > $width) {
            $image->resize($width, null, function ($constraint) {
                $constraint->aspectRatio();
            });
        }

        return $image;
    }

    public function getUrl($path, $expiry = null)
    {
        return Storage::disk($this->getDisk())->temporaryUrl(
            $path,
            $expiry ?? config('fileupload.avatar_users.avatar_url_expire', 60)
        );
    }
}
