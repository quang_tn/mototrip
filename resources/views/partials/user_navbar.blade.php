<nav id="user-nav" class="bg-blue-900 text-white fixed w-full z-40" style="top:56.67px;">
    <div x-data="{ open:  toggleDropdown()}"
         class="container mx-auto px-6 py-2 flex items-center justify-between relative">
        <div>
            <div x-on:click="open = true"
                 x-on:resize.window.debounce="open = toggleDropdown()"
                 class="md:hidden">
                <i class="fas fa-bars text-lg"></i></div>
            <div x-show.transition.in="open"
                 x-on:click.away="open = toggleDropdown()"
                 class="dropdown absolute left-0 px-6 py-2 w-full bg-blue-900 z-50 md:static md:flex md:items-center md:p-0">
                <a href="/admin/albums"
                   class="p-1 font-medium hover:text-orangeMain {{request()->is('admin/albums/*') ? 'active' : null}}">
                    <span><i class="fas fa-camera-retro mr-1"></i> Albums</span></a>
                <a href="/admin/trips/*"
                   class="block ml-0 md:inline p-1 hover:text-orangeMain md:ml-6 font-medium {{request()->is('admin/trips/*') ? 'active' : null}}">
                    <span><i class="fas fa-biking mr-1"></i> Trips</span></a>
                <a href="/admin/coupons/*"
                   class="block ml-0 md:inline p-1 hover:text-orangeMain md:ml-6 font-medium {{request()->is('admin/coupons/*') ? 'active' : null}}">
                    <span><i class="fas fa-gift mr-1"></i> Coupons</span></a>
                <a href="/admin/users/*"
                   class="block ml-0 md:inline p-1 hover:text-orangeMain md:ml-6 font-medium {{request()->is('admin/users/*') ? 'active' : null}}">
                    <span><i class="fas fa-users mr-1"></i> Users</span></a>
                <span x-data="{ openSub1: false }"
                      class="relative">
                   <span x-on:click="openSub1=true"
                         class="block ml-0 md:inline p-1 font-medium hover:text-orangeMain md:ml-6 cursor-pointer {{request()->is('admin/invoices/*') ? 'active' : null}}">
                      <i class="fas fa-receipt mr-1"></i> Invoices
                   </span>
                   <ul x-show.transition.in="openSub1"
                       x-on:click.away="openSub1 = false"
                       class="text-white md:text-gray-700 ml-4 my-2 md:absolute md:z-50 md:bg-white md:w-48 md:pt-4 md:rounded md:p-3 md:border md:border-gay-400 md:shadow-md">
                      <a href="/admin/invoices/cancelled-bookings"><li class="hover:text-orangeMain">Cancelled Bookings</li></a>
                      <a href="/admin/invoices/coming-trips"><li class="mt-1 hover:text-orangeMain">Upcoming Trips</li></a>
                      <a href="/admin/invoices/bookings"><li class="mt-1 hover:text-orangeMain">Bookings</li></a>
                      <a href="/admin/invoices"><li class="mt-1 hover:text-orangeMain">Invoices</li></a>
                   </ul>
                </span>
                <a href="/admin/videos"
                   class="block ml-0 md:inline p-1 font-medium hover:text-orangeMain md:ml-6 {{request()->is('videos/*') ? 'active' : null}}"><span><i class="fas fa-video mr-1"></i> Videos</span></a>
            </div>
        </div>
        <a href="/">
            <img src="/images/apple-touch-icon-72x72.png" width="34px" height="34px" alt="avatar">
        </a>
    </div>
</nav>
