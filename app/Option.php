<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Option extends Model
{
    protected $guarded = [];

    protected static function boot()
    {
        parent::boot();
        static::deleting(function ($model) {
            $model->schedules()->delete();
            $model->bookings()->delete();
        });
    }

    public function optionable()
    {
        return $this->morphTo();
    }

    public function schedules()
    {
        return $this->morphMany(Schedule::class, 'schedulable');
    }

    public function bookings()
    {
        return $this->morphMany(Booking::class, 'bookable');
    }

    public function scheduledFees()
    {
        if (!cache()->has('Option_' . $this->id . '_scheduledFees')) {
            $acceptableGroupPrice = [];
            foreach ($this->schedules as $schedule) {
                foreach ($schedule->prices as $price) {
                    $group = $price->travelerGroup;
                    $tier = get_object_vars(json_decode($price->tier));
                    $lastUpper = 1;
                    foreach ($tier as $upperRange => $fee) {
                        for ($i = $lastUpper; $i <= $upperRange; $i++) {
                            $acceptableGroupPrice[$schedule->id][$group->slug][$i] = $fee;
                        }
                        $lastUpper = $upperRange + 1;
                    }
                    $acceptableGroupPrice[$schedule->id][$group->slug]['max'] = array_key_last($tier);
                }
            }
            cache()->forever('Option_' . $this->id . '_scheduledFees', $acceptableGroupPrice);
        }
        return cache('Option_' . $this->id . '_scheduledFees');
    }

    public function defineStartDate($pricePeriodStart = null)
    {
        if ($this->optionable_type == 'App\Transfer') {
            $sameDateBookable = false;
            for ($h = now('Asia/Ho_Chi_Minh')->hour + 1; $h < 24; $h++) {
                $cutoff = $this->optionable->cutoff;
                if ($h >= 10 && $h <= 22) $cutoff = 5;
                $nextValidTime = now('Asia/Ho_Chi_Minh')->addHours($cutoff);
                if ($nextValidTime->isBefore(now('Asia/Ho_Chi_Minh')->setHour($h)->startOfHour())) {
                    $sameDateBookable = true;
                    break;
                }
            }
            if (!$sameDateBookable) $nextValidTime = now('Asia/Ho_Chi_Minh')->addDay();
        }
        /* if ($nextValidTime->hour > explode(':', $this->departure_time)[0]) $validStartDate = $nextValidTime->addDay();
        else $validStartDate = $nextValidTime; */
        if ($this->optionable_type == 'App\Tour') {
            $nextValidTime = now('Asia/Ho_Chi_Minh')->addHours($this->optionable->cutoff);
            if ($nextValidTime->hour > $this->departureHour()) $nextValidTime = $nextValidTime->addDay();
        }

        if (($pricePeriodStart ?? now('Asia/Ho_Chi_Minh'))->gt($nextValidTime)) $nextValidTime = $pricePeriodStart;
        return $nextValidTime->startOfDay();
    }

    public function departureHour()
    {
        if (count($hour = json_decode($this->times)) > 0) $hour = explode(':', $hour[0])[0];
        else $hour = 0;
        return $hour;
    }
}
