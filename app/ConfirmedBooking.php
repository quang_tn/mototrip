<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ConfirmedBooking extends Model
{
    protected $guarded = [];
    protected $appends = ['paxInfo'];
    protected $casts = [
        'pax' => 'array'
    ];

    public function getPaxInfoAttribute()
    {
        return $this->pax['text'];
    }

    public function scopeFilter($query, $filter)
    {
        return $filter->apply($query);
    }

    public function invoice()
    {
        return $this->belongsTo(Invoice::class);
    }

    public function bookable()
    {
        return $this->morphTo();
    }


}
