<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OtherComment extends Model
{
    protected $guarded = [];
}
