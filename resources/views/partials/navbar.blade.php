<nav id="main-nav" class="bg-white text-gray-700 fixed top-0 w-full z-50">
    <div x-data="{ open:  toggleDropdown()}"
         class="container mx-auto px-3 py-2 flex items-center justify-between relative">
        <a href="/">
            <svg class="w-32 h-auto"
                 xmlns="http://www.w3.org/2000/svg"
                 viewBox="0 0 188 59.75">
                <path
                    d="M158.04 31.75h6.82l-5.14 20.65h-6.81l5.13-20.65zM182.65 32.66a9 9 0 00-4.13-.94h-9.03l-6.85 27.61h6.82l2.21-8.92a9.6 9.6 0 001.56 1c4.8 2.41 10.81.16 13.41-5s.81-11.34-3.99-13.75zm-1.1 11.2a4.05 4.05 0 11-1.65-5.71 4.2 4.2 0 011.65 5.71zM132 24.76h-11.75l-1.72 6.8h3.78a3.42 3.42 0 013.32 4.25l-4.18 16.59h6.82l6.95-27.64zM151.11 33.5h-4.32a3.43 3.43 0 00-3.12 2.36l-.09.34 1.1-4.44h-6.82l-5.14 20.64h6.82l2.37-9.51a3.38 3.38 0 013.29-2.58h6.24l1.7-6.81z"/>
                <circle cx="161.48"
                        cy="24.65"
                        r="3.41"/>
                <path class="cls-1"
                      d="M28.26 3.93l26.18 11.55a4.19 4.19 0 004-.54l11.4-8.06 9.82 14.35h-2.14l-7.65-9.77L56 21.26 28.26 6.12zM52.8 26c-6.41-3.22-14.43-.22-17.91 6.7s-1.09 15.19 5.32 18.41 14.44.22 17.92-6.7S59.22 29.26 52.8 26zm-9.5 19a6.9 6.9 0 01-2.71-9.36 6.9 6.9 0 019.12-3.41 6.9 6.9 0 012.72 9.36A6.91 6.91 0 0143.3 45zM28.29 24.76l-9.36 13.93-4.66-13.93H7.45L.5 52.4h6.82l4.03-16.02 3.04 9.08h6.81l13.9-20.7h-6.81zM76.42 24.76H64.7l-1.7 6.8h3.78a3.42 3.42 0 013.32 4.25L65.9 52.4h6.82l7-27.64zM74 .14h.45l.28.47 22.84 37.93h-2.26L72.6 5.38a3.36 3.36 0 00-2.78-1.47h-2L65.25.14z"/>
                <path class="cls-1"
                      d="M100.48 26a12.83 12.83 0 00-13 1.06l3.69 5.38a6.49 6.49 0 016.22-.29 6.9 6.9 0 012.71 9.36 6.64 6.64 0 11-11.83-5.91 8.71 8.71 0 01.63-1l-3.76-5.49a15.06 15.06 0 00-2.57 3.65c-3.47 6.93-1.09 15.15 5.33 18.37s14.43.22 17.91-6.7 1.09-15.17-5.33-18.43z"/>
            </svg>
        </a>
        <div>
            <div x-on:click="open = true"
                 x-on:resize.window.debounce="open = toggleDropdown()"
                 class="md:hidden">
                <i class="fas fa-bars text-lg"></i></div>
            <div x-show.transition.in="open"
                 x-on:click.away="open = toggleDropdown()"
                 class="dropdown absolute left-0 p-2 w-full bg-white z-50 md:static md:flex md:items-center md:p-0">
                <a href="/about-us"
                   class="uppercase p-1 font-medium hover:text-orangeMain {{request()->is('about-us') ? 'active' : null}}">
               <span>About
                  Us</span></a>
                <span x-data="{ openSub1: false }"
                      class="relative">
               <span x-on:click="openSub1=true"
                     class="block ml-0 md:inline uppercase p-1 font-medium hover:text-orangeMain md:ml-6 cursor-pointer {{request()->is('trips') ? 'active' : null}}">
                  Trips
               </span>
               <ul x-show.transition.in="openSub1"
                   x-on:click.away="openSub1 = false"
                   class="ml-4 my-2 md:absolute md:z-50 md:bg-white md:w-48 md:pt-4 md:rounded md:p-3 md:border md:border-gay-400 md:shadow-md">
                  <a href=""><li class="hover:text-orangeMain">Moto Trips</li></a>
                  <a href=""><li class="mt-1 hover:text-orangeMain">Bike Trips</li></a>
                  <a href=""><li class="mt-1 hover:text-orangeMain">Car Trips</li></a>
                  <a href=""><li class="mt-1 hover:text-orangeMain">Shore Excursion Trips</li></a>
               </ul>
            </span>
                <a href=""
                   class="block ml-0 md:inline uppercase p-1 font-medium hover:text-orangeMain md:ml-6 {{request()->is('bikes') ? 'active' : null}}"><span>Bikes</span></a>
                <a href=""
                   class="block ml-0 md:inline uppercase p-1 font-medium hover:text-orangeMain md:ml-6 {{request()->is('contact-us') ? 'active' : null}}"><span>Contact
                  Us</span></a>
                <user-nav-button :logged-in="{{ json_encode(auth()->check()) }}"
                                 :user="{{ json_encode(auth()->check() ? auth()->user() : null)}}">
                    <li class="mt-1 hover:text-orangeMain">
                        <a href onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">Logout</a>
                    </li>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </user-nav-button>
                <span x-data="{ openSub2: false }"
                      class="relative">
               <a href=""
                  x-on:click.prevent="openSub2=true"
                  class="ml-0 md:ml-6 inline-block"><img src="/images/{{session('currency','vnd')}}.svg"
                                                         style="height:29px"
                                                         alt=""></a>
               <ul x-show.transition.in="openSub2"
                   x-on:click.away="openSub2 = false"
                   class="ml-4 my-2 md:absolute md:z-50 md:bg-white md:w-12 md:pt-4 md:rounded md:p-3">
                   @foreach (array_diff(['usd','vnd'],[session('currency','vnd')]) as $currency)
                       <li><a href="/switchCurrency/{{$currency}}"
                              title="USD"><img src="/images/{{$currency}}.svg"
                                               style="height:29px"
                                               alt=""></a></li>
                   @endforeach

               </ul>
            </span>
            </div>
        </div>
    </div>
</nav>
