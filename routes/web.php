<?php

namespace App;

use App\Http\Controllers\Admin\AlbumController;
use App\Http\Controllers\Auth\SocialLoginController;
use App\Http\Controllers\Auth\VerificationController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\Admin\AdminController;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('pages.index');
});

Route::get('redirect', [SocialLoginController::class, 'redirect']);
Route::get('callback', [SocialLoginController::class, 'callback']);

Route::namespace('Admin')->prefix('admin')->middleware('isAdmin')->group(function () {
    Route::get('dashboard', [AdminController::class, 'index']);

    Route::get('albums', [AlbumController::class, 'index']);
    Route::get('albums/{album}/downloadPhotos', [AlbumController::class, 'downloadPhotos']);
    Route::post('albums', [AlbumController::class, 'store']);
    Route::put('albums/{album}', [AlbumController::class, 'update']);
    Route::get('albums/{album}/photos', [AlbumController::class, 'photos']);
    Route::get('albums/{album}/videos', [AlbumController::class, 'videos']);
    Route::post('albums/{album}/addVideo', [AlbumController::class, 'addVideo']);
    Route::delete('albums/{album}/removeVideo/{video_id}', [AlbumController::class, 'removeVideo']);
    Route::get('albums/{album}/photos/add', [AlbumController::class, 'addPhotos']);
    Route::post('photos/storeOrder', [AlbumController::class, 'storeOrder']);
    Route::post('albums/photos', [AlbumController::class, 'storePhoto']);
    Route::put('photos/{photo}', [AlbumController::class, 'savePhotoName']);
    Route::delete('albums/{album}', [AlbumController::class, 'destroy']);
    Route::delete('albums/{album}/photos/{photo}', [AlbumController::class, 'removePhoto']);

    Route::get('trips', 'Admin\TourController@index');
    Route::get('trips/create', 'Admin\TourController@create');
    Route::post('trips', 'Admin\TourController@store');
    Route::get('trips/{trip}/edit', 'Admin\TourController@edit');
    Route::get('trips/{trip}/options', 'Admin\TourController@options');
    Route::put('trips/{trip}', 'Admin\TourController@update');
    Route::delete('trips/{trip}', 'Admin\TourController@destroy');
    Route::put('trips/{trip}/updateStatus', 'Admin\TourController@updateStatus');
    Route::post('trips/{trip}/{url?}', 'Admin\TourController@storePhoto');

    Route::get('coupons', 'Admin\CouponController@index');
    Route::get('coupons/cleanUp', 'Admin\CouponController@cleanUp');
    Route::delete('coupons/{coupon}', 'Admin\CouponController@destroy');
    Route::post('coupons', 'Admin\CouponController@store');

    Route::get('users', 'UserController@listUsers')->middleware('isAdmin');
    Route::put('users/{user}/verifyEmail', 'UserController@verifyEmail')->middleware('isAdmin');
    Route::get('users/searchByName', 'UserController@searchByName')->middleware('isAdmin');

    Route::get('invoices/cancelled-bookings', 'Admin\InvoiceController@cancelledBookings');
    Route::get('invoices/cancelled-bookings/{booking}/sendEmail', 'Admin\InvoiceController@notifyUser');
    Route::get('invoices/coming-trips', 'Admin\InvoiceController@comingTrips');
    Route::get('invoices/bookings', 'Admin\InvoiceController@bookings');
    Route::get('invoices', 'Admin\InvoiceController@invoices');
    Route::get('invoices/{invoice}/view', 'Admin\InvoiceController@viewInvoice');
    Route::get('invoices/{invoice}/download', 'Admin\InvoiceController@downloadInvoice');
    Route::post('invoices/filter', 'Admin\InvoiceController@filterInvoices');
    Route::get('export/invoices', 'Admin\InvoiceController@downloadInvoices');
    Route::get('export/comingTrips/{date}', 'Admin\InvoiceController@downloadComingTour');

    Route::get('options/{option}/edit', 'Admin\OptionController@edit');
    Route::post('options', 'Admin\OptionController@store');
    Route::put('options/{option}', 'Admin\OptionController@update');
    Route::delete('options/{option}', 'Admin\OptionController@destroy');

    Route::get('videos', 'VideoController@index');
    Route::post('videos', 'VideoController@store');
    Route::put('videos/{video}', 'VideoController@update');
    Route::delete('videos/{video}', 'VideoController@destroy');
});

Route::get('/user', function () {
    return 1;
})->middleware('auth');

Route::get('/switchCurrency/{currency}', [HomeController::class, 'switchCurrency']);

Route::view('/trips/danang-food-trip', 'pages.tour');

Route::get('verify/resend', [VerificationController::class, 'resend'])->middleware('auth');
Auth::routes(['verify' => true]);

Route::group(['middleware' => 'auth'], function(){
    Route::view('about-us', 'pages.about-us')->middleware(['verified']);
});

Route::get('upload', function() {
    $content = file_get_contents(public_path('images/12.jpg'));
    return Storage::putFileAs('images', new File(public_path('images/12.jpg')), 'po1.jpg');
});

