/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require("./bootstrap");

window.Vue = require("vue");
import Vuex from 'vuex'
Vue.use(Vuex)

import VueAwesomeSwiper from "vue-awesome-swiper";
import "swiper/css/swiper.css";
Vue.use(VueAwesomeSwiper);

import VueSocialSharing from 'vue-social-sharing'
Vue.use(VueSocialSharing);

import "alpinejs";

import VueScrollTo from "vue-scrollto";
Vue.use(VueScrollTo, {
    container: "body",
    duration: 500,
    easing: "ease",
    offset: -57,
    force: true,
    cancelable: true,
    onStart: false,
    onDone: false,
    onCancel: false,
    x: false,
    y: true
});

import Toasted from 'vue-toasted';
Vue.use(Toasted, {
    position: 'bottom-center',
    iconPack: 'fontawesome',
    theme: 'bubble',
    duration: 3000
})


/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component("slider", require("./components/Slider.vue").default);
Vue.component(
    "testimonials-slider",
    require("./components/TestimonialsSlider.vue").default
);
Vue.component(
    "three-at-a-time-slider",
    require("./components/ThreeAtATimeSlider.vue").default
);
Vue.component(
    "similar-tours",
    require("./components/SimilarTours.vue").default
);
Vue.component(
    "modal",
    require("./components/Modal.vue").default
);
Vue.component(
    "login-register",
    require("./components/LoginRegister.vue").default
);
Vue.component(
    "reset-password",
    require("./components/ResetPassword.vue").default
);
Vue.component(
    "user-nav-button",
    require("./components/UserNavButton.vue").default
);
Vue.component(
    "toast-alert",
    require("./components/ToastAlert.vue").default
);
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
import storeData from './store/index.js'
const store = new Vuex.Store(storeData)

const app = new Vue({
    el: "#app",
    store,
    mounted() {

    }
});
