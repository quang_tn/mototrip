<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConfirmedBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('confirmed_bookings', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('invoice_id');
            $table->unsignedDecimal('total');
            $table->text('request')->nullable();
            $table->date('date');
            $table->json('pax');
            $table->string('lead_traveler');
            $table->string('pickup')->nullable();
            $table->tinyInteger('status');
            $table->morphs('bookable');
            $table->json('option')->nullable();
            $table->tinyInteger('vehicle');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('confirmed_bookings');
    }
}
