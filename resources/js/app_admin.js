require("./bootstrap");

window.Vue = require("vue");
import Vuex from 'vuex'
import "alpinejs";

import Toasted from 'vue-toasted';
import storeAdminData from './store/admin.js'

Vue.use(Vuex)

Vue.use(Toasted, {
    position: 'bottom-center',
    iconPack: 'fontawesome',
    theme: 'bubble',
    duration: 3000
})

var moment = require("moment-timezone");
moment.tz.setDefault("Asia/Ho_Chi_Minh");
Object.defineProperty(Vue.prototype, 'moment', {
    value: moment
})
import 'vue-croppa/dist/vue-croppa.css';
import Croppa from 'vue-croppa';
Vue.use(Croppa);

Vue.component(
    "toast-alert",
    require("./components/ToastAlert.vue").default
);
Vue.component(
    "modal",
    require("./components/Modal.vue").default
);
Vue.component("albums-listing", require("./components/Admin/AlbumsListing.vue").default);
Vue.component(
    "login-register",
    require("./components/LoginRegister.vue").default
);
Vue.component(
    "reset-password",
    require("./components/ResetPassword.vue").default
);
Vue.component(
    "user-nav-button",
    require("./components/UserNavButton.vue").default
);

const store = new Vuex.Store(storeAdminData)

const app = new Vue({
    el: '#app',
    store
})
