<?php

use App\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Create Admin User
        \DB::table('users')->insert([
            'title' => 'Mr.', 'firstname' => 'Quang', 'lastname' => 'Tran', 'avatar' => '/images/apple-touch-icon-114x114.png',
            'email' => 'it@iut.vn', 'password' => bcrypt('Askforpass@123')
        ]);

        $count = 5;
        factory(User::class, $count)->create();
    }
}
