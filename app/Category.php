<?php

namespace App;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Category
 * @package App
 */
class Category extends Model
{
    use Sluggable;

    public const STATUS = [
        'ACTIVE' => 1,
        'INACTIVE' => 0
    ];

    protected $fillable = [
        'name'
    ];

    public $timestamps = false;

    protected static function boot()
    {
        parent::boot();
        static::created(function ($model) {
            cache()->forget('categories');
        });
    }

    /**
     * @return string[][]
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    /* Set wildcard name instead of default ID */
    /**
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphToMany
     */
    public function tours()
    {
        return $this->morphedByMany('App\Tour', 'categorizable');
    }
}
