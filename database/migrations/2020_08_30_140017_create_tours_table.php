<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateToursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tours', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('album_id');
            $table->tinyInteger('type')->comment('Half day, Full day or Multi days Tour');
            $table->string('name');
            $table->string('slug')->unique();
            $table->text('brief');
            $table->string('image')->nullable();
            $table->boolean('hot')->default(false);
            $table->boolean('active')->default(false);
            $table->string('departure_time')->nullable();
            $table->tinyInteger('cutoff');
            $table->tinyInteger('min_quantity');
            $table->tinyInteger('max_quantity');
            $table->text('include')->nullable();
            $table->text('exclude')->nullable();
            $table->text('notice')->nullable();
            $table->tinyInteger('day_number')->default(1);
            $table->boolean('pickup')->default(false);
            $table->tinyInteger('cancellation');
            $table->json('extra_info')->nullable();
            $table->json('vehicle_tier')->nullable();
            $table->unsignedBigInteger('comments_count')->default(0);
            $table->softDeletes();
            $table->timestamps();
            $table->foreign('album_id')->references('id')->on('albums');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tours');
    }
}
