<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    protected $guarded = [];

    /**
     * Get all of the tours that are assigned this video.
     */
    public function tours()
    {
        return $this->morphedByMany('App\Tour', 'videoable');
    }

    /**
     * Get all of the albums that are assigned this video.
     */
    public function albums()
    {
        return $this->morphedByMany('App\Album', 'videoable');
    }
}
