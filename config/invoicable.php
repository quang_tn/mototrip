<?php

return [
    'default_currency' => 'USD',
    'default_status' => 'Not Confirmed',
    'locale' => 'nl_NL',
];
