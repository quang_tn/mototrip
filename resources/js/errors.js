class Errors {
	constructor() {
		this.errors = {};
	}
	getError(field) {
		if(this.errors[field]) {
			return this.errors[field][0];
		}
	}
	getClass(field) {
		if(this.errors[field]) {
			return 'red';
		}
	}
	record(error) {
		this.errors = error;
	}
	reset() { this.errors = {} }
}

export default Errors