<?php

namespace Tests\Mocking;

use Laravel\Socialite\Contracts\Factory;
use Faker\Generator as Faker;
use Mockery;

class SocialiteMock
{
    public static function fake($abstractUser)
    {
        /*app()->instance(Factory::class, Mockery::mock(Factory::class, function ($mock) {
            $mock->shouldReceive('driver')->once()->andReturn(self::provider());
        }));*/
        $provider = self::provider();
        app()->instance(Factory::class, Mockery::mock(Factory::class, function ($mock) use ($provider) {
            $mock->shouldReceive('driver')->andReturn($provider);
        }));
        $provider->shouldReceive('user')->andReturn($abstractUser);
    }

    public static function provider()
    {
        return Mockery::mock('Laravel\Socialite\Contracts\Provider');
    }

    public static function abstractUser($params = [], $OAuth = 2)
    {
        $faker = app(Faker::class);
        $data = [
            'id' => $faker->uuid,
            'nickname' => $faker->userName,
            'name' => $faker->name,
            'email' => $faker->email,
            'avatar' => $faker->imageUrl()
        ];
        if(!empty($params)) $data = array_merge($data, $params);
        $abstractClass = $OAuth === 1 ? 'Laravel\Socialite\One\User' : 'Laravel\Socialite\Two\User';
        $abstractUser = Mockery::mock($abstractClass);
        $abstractUser
            ->shouldReceive('getId')
            ->andReturn($data['id'])
            ->shouldReceive('getNickName')
            ->andReturn($data['nickname'])
            ->shouldReceive('getName')
            ->andReturn($data['name'])
            ->shouldReceive('getEmail')
            ->andReturn($data['email'])
            ->shouldReceive('getAvatar')
            ->andReturn($data['avatar']);
        return $abstractUser;
    }
}
