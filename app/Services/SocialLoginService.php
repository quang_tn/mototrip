<?php

namespace App\Services;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use App\SocialIdentity;
use App\User;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Contracts\User as ProviderUser;

class SocialLoginService
{
    public function login(ProviderUser $providerUser, $provider)
    {
        list($id, $email, $name, $avatar) = $this->getSocialIdentities($providerUser);

        if ($account = SocialIdentity::where('provider', $provider)->where('provider_user_id', $id)
            ->first()) {
            return $this->sendSuccessLogin($account->user_id);
        }

        if (!$name || !$email) {
            return $this->sendFailedLogin($id);
        }

        if (!$user = User::firstWhere('email', $email)) {
            $avatar = $this->getAvatar($avatar);
            $user = $this->createNewUser($email, $name, $avatar);
        }

        SocialIdentity::create([
            'provider_user_id' => $id,
            'provider' => $provider,
            'user_id' => $user->id
        ]);

        return $this->sendSuccessLogin($user->id);
    }

    /**
     * @param string $email
     * @param string $name
     * @param string $avatar
     * @param string $default_password
     * @return mixed
     */
    private function createNewUser(string $email, string $name, string $avatar)
    {
        $user = User::create([
            'email' => $email,
            'firstname' => $name,
            'avatar' => $avatar,
        ]);
        $user->sendEmailVerificationNotification();
        return $user;
    }

    /**
     * @param ProviderUser $providerUser
     * @param $user
     * @return mixed|string
     */
    private function getAvatar($avatar)
    {
        $fileTemp = getImageFromURL($avatar);
        return Storage::putFile(config('mototrip.file_upload.avatars.path'), $fileTemp);
    }

    /**
     * @param ProviderUser $providerUser
     * @return array
     */
    private function getSocialIdentities(ProviderUser $providerUser): array
    {
        $id = $providerUser->getId();
        $email = $providerUser->getEmail();
        $name = $providerUser->getName();
        $avatar = $providerUser->getAvatar();
        return array($id, $email, $name, $avatar);
    }

    /**
     * @param int $userId
     * @return Redirector
     */
    private function sendSuccessLogin(int $userId)
    {
        $redirectTo = '/';
        Auth::loginUsingId($userId);
        if(session()->has('prevUrl')) $redirectTo = session('prevUrl');
        return redirect($redirectTo)->with([
            'message' => 'You are logged in successfully',
            'type' => 'success'
        ]);
    }

    /**
     * @param $id
     * @return Redirector
     */
    private function sendFailedLogin($id)
    {
        return redirect('/')->with([
            'message' => 'Whoops, we can not get enough information from your social profile. Please use this signup
                form instead.',
            'type' => 'warning', 'showSignup' => true, 'providerUserID' => $id
        ]);
    }
}
