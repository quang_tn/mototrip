<?php

namespace App;

use App\Services\UploadMedia\Facades\ImageHelper;
use App\Traits\RecordsActivity;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    use RecordsActivity;

    protected $guarded = [];
    protected $casts = [
        'images' => 'array'
    ];
    protected $with = ['replies', 'user'];

    protected static function boot()
    {
        parent::boot();
        static::deleting(function ($model) {
            $model->replies->each->delete(); //Del child replies
            //Update comments_count
            $model->commentable->decrement('comments_count');
            //Delete uploaded photos belong to this comment
            if (!empty($model->images)) {
                foreach ($model->images as $photo) {
                    ImageHelper::delete(public_path(config('mototrip.file_upload.comments.path')), $photo);
                    ImageHelper::delete(public_path(config('mototrip.file_upload.comments.path')), 'thumb_'.$photo);
                }
            }
        });
    }

    /**
     * Get all of the owning commentable models.
     */
    public function commentable()
    {
        return $this->morphTo();
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    //Get sub comments
    public function replies()
    {
        return $this->hasMany('App\Comment', 'parent_comment_id')->latest();
    }
}
