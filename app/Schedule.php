<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
    protected $guarded = [];
    protected $with=['prices'];
    protected $casts = [
        'on' => 'array',
        'not_on' => 'array'
    ];

    protected static function boot()
    {
        parent::boot();
        static::deleting(function ($model) {
            $model->prices()->delete();
        });
    }

    public function prices()
    {
        return $this->hasMany('App\Price')->orderBy('traveler_group_id');
    }
}
