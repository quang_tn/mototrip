<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\SocialLoginRequest;
// use App\Option;
use App\Services\SocialLoginService;
use App\User;
use Socialite;

/**
 * Class SocialLoginController
 * @package App\Http\Controllers\Auth
 */
class SocialLoginController extends Controller
{
    /**
     * Redirect to social page
     * @param SocialLoginRequest $request
     * @return mixed
     */
    public function redirect(SocialLoginRequest $request)
    {
        if ($request->filled('provider')) {
            $provider = $request->provider;
            $request->session()->put('provider', $provider);
            if (!session()->has('prev_url')) $request->session()->put('prev_url', url()->previous()); //Flash to /callback
            return Socialite::driver($provider)->redirect();
        }
    }

    public function callback(SocialLoginService $service)
    {
        try {
            $providerUser = Socialite::driver(session('provider'))->user();
            return $service->login($providerUser, session('provider'));
        } catch (\Exception $e) {
            dd($e);
            return redirect('/')->with(['message' => 'Login unsuccessful, please try other login options!',
                'type' => 'error']);
        }
    }
}
