<?php

namespace App;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphToMany;
use Illuminate\Support\Facades\File;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class Album extends Model implements HasMedia
{
    use Sluggable;
    use HasMediaTrait;

    protected $guarded = [];
    protected $casts = [
        'is_activated' => 'boolean'
    ];
    public const COLLECTION = [
        'PHOTOS' => 'photos',
        'COVER' => 'cover'
    ];
    public $timestamps = false;

    protected static function boot()
    {
        parent::boot();

        static::deleting(function ($album) {
            $album->videos()->detach();
        });
    }

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function getCoverAttribute()
    {
        return $this->getFirstMediaUrl(self::COLLECTION['COVER']);
    }

    /**
     * Get all of the videos for the album.
     * @return MorphToMany
     */
    public function videos()
    {
        return $this->morphToMany('App\Video', 'videoable');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tours()
    {
        return $this->hasMany('App\Tour');
    }

    public function registerMediaCollections()
    {
        $this
            ->addMediaCollection(self::COLLECTION['COVER'])
            ->singleFile();
    }
}
