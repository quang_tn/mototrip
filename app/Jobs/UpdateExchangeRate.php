<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;

class UpdateExchangeRate implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Storage::put('vcb_rates.xml', Http::withOptions([
            'verify' => false,
        ])->get('http://portal.vietcombank.com.vn/Usercontrols/TVPortal.TyGia/pXML.aspx?b=1')->body());

        $xml = simplexml_load_file(public_path("vcb_rates.xml")) or die("Error: Cannot create xml file");

        foreach ($xml->Exrate as $currency) {
            if ($currency['CurrencyCode'] == 'USD') {
                Cache::put('usdRate', (float) str_replace(',', '', $currency['Buy']->__toString()), now()->addHours(24));
                break;
            }
        }
    }
}
