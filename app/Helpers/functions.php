<?php

use Ramsey\Uuid\Uuid;
use Symfony\Component\Mime\MimeTypes;

if (!function_exists('currentUserLogin')) {

    /**
     * Get current user login
     *
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    function currentUserLogin()
    {
        return \Illuminate\Support\Facades\Auth::user();
    }
}

if (!function_exists('toPgArray')) {

    /**
     * Convert to array postgres
     *
     * @param array $data Data
     *
     * @return bool
     */
    function toPgArray(array $data)
    {
        $result = [];
        foreach ($data as $value) {
            if (is_array($value)) {
                $result[] = toPgArray($value);
            } else {
                $value = str_replace('"', '\\"', $value);
                if (!is_numeric($value)) {
                    $value = '"' . $value . '"';
                }
                $result[] = $value;
            }
        }

        return '{' . implode(",", $result) . '}';
    }

    if (!function_exists('fcm')) {
        function fcm()
        {
            return app('fcm');
        }
    }

    if (!function_exists('getAvatarUrl')) {
        function getAvatarUrl(string $path, $expiry = null)
        {
            \ImageHelper::setDisk(config('fileupload.avatar_users.disk'));
            return \ImageHelper::getUrl($path, $expiry);
        }
    }

    if (!function_exists('getMimeType')) {
        function getMimeType(string $fileName)
        {
            $mimeTypes = new MimeTypes();

            $fileExt = pathinfo($fileName, PATHINFO_EXTENSION);

            if (empty($fileExt)) {
                return null;
            }

            return $mimeTypes->getMimeTypes($fileExt) ? current($mimeTypes->getMimeTypes($fileExt)) : null;
        }
    }

    if (!function_exists('uuid')) {
        function uuid()
        {
            return Uuid::uuid4()->toString();
        }
    }

    if (!function_exists('randomInArray')) {
        function randomInArray(array $array)
        {
            return $array[array_rand($array)];
        }
    }

    if (!function_exists('randomString')) {
        function randomString(int $length = 0)
        {
            if ($length === 0) {
                $length = mt_rand(10, 100);
            }

            $characters = ' 0123456789 abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $charactersLength = strlen($characters);
            $randomString = '';
            for ($index = 0; $index < $length; $index++) {
                $randomString .= $characters[rand(0, $charactersLength - 1)];
            }

            return $randomString;
        }
    }

    if (!function_exists('getImageFromURL')) {
        function getImageFromURL(string $URL)
        {
            $tempPath = '/tmp/tmpfile';

            if (!file_exists($tempPath)) {
                mkdir($tempPath);
            }

            $tempFilePath = $tempPath . basename($URL) . time();

            fopen($tempFilePath, "w") or die("Error: Unable to open file.");

            $fileContents = file_get_contents($URL);

            file_put_contents($tempFilePath, $fileContents);

            return $tempFilePath;
        }
    }

    if (!function_exists('getPageSize')) {
        function getPageSize($name)
        {
            return config(
                "request.pagination.{$name}",
                config('request.pagination.default', 15)
            );
        }
    }
}
