<?php

if (!defined('UPLOAD_AVATAR_IMAGE')) {
    /**
     * Upload file with type is document
     * User can upload file with type is PDF, JPG, JPEG, PNG
     */
    define('UPLOAD_AVATAR_IMAGE', 1);
}

if (!defined('UPLOAD_COVER_IMAGE')) {
    /**
     * Upload image file
     */
    define('UPLOAD_COVER_IMAGE', 2);
}

if (!defined('UPLOAD_VIDEO_TYPE')) {
    /**
     * Upload file with type is video
     * User can upload file with type is FLV, MP4, MPV
     */
    define('UPLOAD_VIDEO_TYPE', 3);
}

if (!defined('APP_BUNDLE_ID')) {
    define('APP_BUNDLE_ID', env('APP_BUNDLE_ID', 'zatsudan'));
}

if (!defined('IOS_BUNDLE_ID')) {
    define('IOS_BUNDLE_ID', env('IOS_BUNDLE_ID', 'jp.zatsudan.dev'));
}

if (!defined('ANDROID_BUNDLE_ID')) {
    define('ANDROID_BUNDLE_ID', env('ANDROID_BUNDLE_ID', 'jp.zatsudan.dev'));
}

if (!defined('VERIFY_EMAIL_EXPIRES')) {
    define('VERIFY_EMAIL_EXPIRES', env('VERIFY_EMAIL_EXPIRES', 1440)); // 24 * 60;
}
