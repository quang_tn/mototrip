<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Invoice;
use App\Tour;
use App\User;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function index()
    {
        $statistics = [
            'trips_count' => Tour::count(),
            'invoices_count' => Invoice::count(),
            'users_count' => User::count()
        ];
        return view('admin.dashboard', compact('statistics'));
    }
}
