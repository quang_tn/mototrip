<?php

namespace App;

use App\Notifications\ResetPasswordNotification;
use App\Notifications\VerifyEmail;
use App\Services\UploadMedia\Facades\ImageHelper;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;

    protected $guarded = [];
    protected $appends = ['isAdmin'];
    protected $casts = [
        'email_verified_at' => 'datetime',
        'settings' => 'array'
    ];
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            $model->settings = [
                'couponChanged' => true,
                'newTourAdded' => true,
                'newTransferAdded' => true
            ];
        });
        static::deleting(function ($model) {
            Activity::where('user_id', $model->id)->get()->each->delete();
            ImageHelper::delete($model->avatar);
            $model->bookings()->delete();
            $model->favorites()->delete();
            $model->invoices->each(function ($i) {
                $i->confirmedBookings()->delete();
                $i->delete();
            });
            $model->socialIdentities()->delete();
            $model->comments()->delete();
        });
    }

    public function invoices()
    {
        return $this->hasMany(Invoice::class);
    }

    public function getIsAdminAttribute()
    {
        return $this->isAdmin();
    }

    /* Check user belongs to ADMIN group */
    public function isAdmin()
    {
        return in_array($this->email, config('mototrip.administrators'));
    }

    public function getAvatarAttribute($avatar)
    {
        return $avatar ?: 'apple-touch-icon-72x72.png';
    }

    public function sendEmailVerificationNotification()
    {
        $this->notify(new VerifyEmail());
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }

    public function bookings()
    {
        return $this->hasMany('App\Booking')->latest();
    }

    public function comments()
    {
        return $this->hasMany('App\Comment')->latest();
    }

    public function confirmedBookings()
    {
        return $this->hasMany('App\ConfirmedBooking')->latest();
    }

    public function favorites()
    {
        return $this->hasMany('App\Favorite')->latest();
    }

    public function getTitleAttribute($title)
    {
        return $title ?: 'Mr/Ms';
    }

    public function socialIdentities()
    {
        return $this->hasMany(SocialIdentity::class);
    }
}
