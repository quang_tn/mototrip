<?php

namespace App;

use App\Traits\RecordsActivity;
use Dompdf\Dompdf;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\View;
use Symfony\Component\HttpFoundation\Response;

class Invoice extends Model
{
    use RecordsActivity;

    protected $guarded = [];

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->currency = config('invoicable.default_currency', 'EUR');
            $model->status = config('invoicable.default_status', 'concept');
            $model->reference = $model->generate();
        });
    }

    public function scopeFilter($query, $filter)
    {
        return $filter->apply($query);
    }

    public function confirmedBookings()
    {
        return $this->hasMany('App\ConfirmedBooking')->latest();
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Create an invoice download response.
     *
     * @param array $data
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function download(array $data = [])
    {
        $filename = $this->reference . '.pdf';
        return new Response($this->pdf(['logo' => 1]), 200, [
            'Content-Description' => 'File Transfer',
            'Content-Disposition' => 'attachment; filename="' . $filename . '"',
            'Content-Transfer-Encoding' => 'binary',
            'Content-Type' => 'application/pdf',
        ]);
    }

    //Get to the model owns this invoice (User in this case)

    /**
     * Capture the invoice as a PDF and return the raw bytes.
     *
     * @param array $data
     * @return string
     */
    public function pdf(array $data = [])
    {
        if (!defined('DOMPDF_ENABLE_AUTOLOAD')) {
            define('DOMPDF_ENABLE_AUTOLOAD', false);
        }

        if (file_exists($configPath = base_path() . '/vendor/dompdf/dompdf/dompdf_config.inc.php')) {
            require_once $configPath;
        }
        /* $data = $this->createQRCode(); */
        $dompdf = new Dompdf;
        $dompdf->loadHtml($this->view($data)->render());
        $dompdf->set_options(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true]);
        $dompdf->render();
        //Remove QR Code file
        $output = $dompdf->output();
        /* \File::delete($data['qrcode']); */
        return $output;
    }

    public function view(array $data = [])
    {
        return View::make('vendor.invoicable.receipt'/* 'invoicable::receipt' */, array_merge($data, [
            'invoice' => $this
        ]));
    }

    protected function generate()
    {
        $date = \Carbon\Carbon::now();
        return $date->format('Y-m-d') . '-' . $this->generateRandomCode();
    }

    protected function generateRandomCode()
    {
        $pool = 'ABCDEFGHJKLMNPQRSTUVWXYZ23456789';
        return substr(str_shuffle(str_repeat($pool, 6)), 0, 6);
    }
}
