<?php

namespace App;

use App\Traits\CommonFunctions;
use App\Traits\Favoritable;
use Illuminate\Database\Eloquent\Model;

class Tour extends Model
{
    use Favoritable, CommonFunctions;

    protected $guarded = [];
    protected $hidden = ['favorites'];
    protected $with = ['locations'];
    protected $appends = ['averageRating'];

    protected static function boot()
    {
        parent::boot();
        static::deleting(function ($model) {
            $model->categories()->detach(); //Unlink all linked categories
            $model->confirmedBookings->each(function ($cb) {
                $cb->invoice->delete(); //Remove invoice belongs to confirmedBooking
                $cb->delete(); //Remove confirmedBooking
            });
            $model->couponSchedules()->delete();
            $model->itineraries()->delete();
            $model->locations()->detach();
            $model->options()->delete();
            $model->surchargeDates()->detach(); //Unlink all surchargeDates
        });
    }

    public function scopeFilter($query, $filter)
    {
        return $filter->apply($query);
    }

    public function categories()
    {
        return $this->morphToMany(Category::class, 'categorizable');
    }

    public function album()
    {
        return $this->belongsTo(Album::class);
    }

    public function locations()
    {
        return $this->belongsToMany(Location::class);
    }

    public function itineraries()
    {
        return $this->hasMany(Itinerary::class);
    }


    public function videos()
    {
        return $this->morphToMany(Video::class, 'videoable');
    }

    public function seatsLeft() //For only dates that had confirmedBookings
    {
        if (!cache()->has('tour_' . $this->id . '_seatsleft')) {
            $r = [];
            $bookings = $this->confirmedBookings->groupBy('date');
            foreach ($bookings as $date => $books) {
                $r[$date] = $this->max_quantity - $books->sum->totalPax;// - $books->sum->adults - $books->sum->children;
            }
            cache()->forever('tour_' . $this->id . '_seatsleft', $r);
        }
        return cache('tour_' . $this->id . '_seatsleft');
    }

    public function getDepartureAttribute()
    {
        return $this->departure();
    }

    public function departure()
    {
        return $this->locations[0]->name;
    }

    public function getPriceAttribute($price)
    {
        return session('currency') === 'usd' ? round($price / cache('usdRate'), 2) : $price;
    }
}
