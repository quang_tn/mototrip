module.exports = {
  displayCategories(list, shortenWord = 'Tour') {
    let txt = ''
    if (list.length > 1) {
      list.forEach((item, index) => {
        txt +=
          item.name.split(shortenWord)[0] +
          (index < list.length - 1 ? ' - ' : '')
      })
      txt += shortenWord
    } else if (list.length === 1) {
      txt = list[0].name
    }
    return txt
  },
  displayLocations(locations) {
    let txt = ''
    locations.forEach((loc, index) => {
      txt += loc.name + (index < locations.length - 1 ? ' - ' : '')
    })
    return txt
  },
  tourLength(dayNumber) {
    if (dayNumber === 1) return 'Day Tour'
    if (dayNumber === 2)
      return dayNumber + ' days/' + (dayNumber - 1) + ' night'
    if (dayNumber > 2) return dayNumber + ' days/' + (dayNumber - 1) + ' nights'
  },
  shorten(str, maxLen, separator = ' ') {
    if (str.length <= maxLen) return str
    return str.substr(0, str.lastIndexOf(separator, maxLen)) + `... `
  }
}
