<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    protected $guarded = [];

    protected $appends = ['paxInfo', 'couponTier', 'total'];

    protected $casts = [
        'pax' => 'array'
    ];

    public function path()
    {
        return '/tours/' . $this->bookable->slug . '/bookings/' . $this->booking_id;
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function bookable()
    {
        return $this->morphTo();
    }

    public function getPaxInfoAttribute()
    {
        return $this->pax_info();
    }

    public function pax_info()
    {
        $r = '';
        foreach ($this->pax as $name => $number) {
            $r .= $number . ' ' . trans_choice('custom.' . $name, $number) . ' ';
        }
        return rtrim($r);
    }

/*    public function optionableClassName()
    {
        return (new \ReflectionClass($this->bookable->optionable))->getShortName();
    }*/

    public function getCouponTierAttribute()
    {
        return $this->bookable->optionable->scheduledDates[$this->date]['coupon'] ?? null;
    }

    public function getTotalAttribute()
    {
        $scheduleId = $this->bookable->schedules->first()->id;
        // $this->bookable->optionable->scheduledDates[$this->date]['surcharge'] ?? null;

        $groupFees = $this->bookable->scheduledFees()[$scheduleId];
        $total = 0;
        foreach ($this->pax as $name => $number) {
            $subtotal = 0;
            $discount = 0;
            $surcharge = 0;
            $vehicleSurcharge = 0;
            $subtotal += $number != 0 ? ($this->vehicle ? $groupFees[$name][$number] : $number * $groupFees[$name][$number]) : 0;
            if ($this->couponTier) $discount = $subtotal * (isset($this->couponTier[$name]) ? $this->couponTier[$name] : 0) / 100;
            if ($surcharge) $surcharge = $surcharge['type'] == 1 ? ((json_decode($surcharge['surcharge'])->{$name} ?? 0) * $number) : ($subtotal * (json_decode(($surcharge['surcharge'])->{$name} ?? 0) / 100));
            //If App\Transfer
            if ($this->vehicle) {
                $vehicleTier = json_decode($this->bookable->optionable->vehicle_tier)->{$this->vehicle};
                $vehicleSurcharge = $vehicleTier->type == 1 ? $vehicleTier->amount : $subtotal * $vehicleTier->amount / 100;
            }
            $total += $subtotal - $discount + $surcharge + $vehicleSurcharge;
        }
        return $total;
    }
}
