<?php

namespace App\Http\Controllers\Admin;

use App\Album;
use App\Http\Requests\CreateAlbumRequest;
use App\Http\Requests\UpdateAlbumRequest;
use App\Http\Requests\UploadPhotoRequest;
use App\Http\Resources\AlbumResource;
use App\Location;
use App\Photo;
use App\Repositories\AlbumRepositoryEloquent;
use Illuminate\Http\Request;
use Intervention\Image\ImageManagerStatic as Image;
use Prettus\Repository\Criteria\RequestCriteria;
use Spatie\ImageOptimizer\OptimizerChainFactory;
use App\Http\Controllers\Controller;

class AlbumController extends Controller
{
    private $albumRepository;

    public function __construct(AlbumRepositoryEloquent $albumRepository)
    {
        $this->albumRepository = $albumRepository;
        $this->albumRepository->pushCriteria(app(RequestCriteria::class));
    }

    public function index()
    {
        $albums = $this->albumRepository->orderByDesc('id')->get();
        $albums = AlbumResource::collection($albums);

        if (request()->expectsJson()) {
            return $albums;
        }

        return view('admin.albums.index', compact('albums'));
    }

    public function store(CreateAlbumRequest $request)
    {
        $album = $this->albumRepository->create($request->only('name'));
        $this->storePhoto($album, Album::COLLECTION['COVER'], $request->get('filename'));

        return new AlbumResource($album->fresh()->load('tours:album_id,name'));
    }

    public function update(Album $album, UpdateAlbumRequest $request)
    {
        $album->update($request->validated());

        if ($request->hasFile('image')) {
            $this->storePhoto($album, Album::COLLECTION['COVER'], $request->get('filename'));
        }

        return new AlbumResource($album->fresh()->load('tours:album_id,name'));
    }

    public function destroy(Album $album)
    {
        $album->delete();
        return response('Album removed', 200);
    }

    public function storePhoto(Album $album, string $collection, string $fileName)
    {
        $album->addMediaFromRequest('image')
            ->usingFileName($fileName)
            ->toMediaCollection($collection);
    }

    public function photos(Album $album)
    {
        //$photos = $album->photos;
        $album = $album->load('photos');
        return view('admin.albums.photos', compact('album'));
    }

    public function addPhotos(Album $album)
    {
        return view('admin.albums.add_photos', compact('album'));
    }

    public function savePhotoName(Photo $photo)
    {
        $photo->update(['name' => request('name')]);
        return response(['flash' => 'Photo name updated']);
    }

    public function removePhoto(Album $album, Photo $photo)
    {
        $photo->delete();
        $album->photos()->detach($photo->id);
        \File::delete(public_path('/images/albums/' . $album->slug . '/' . $photo->url));
        \File::delete(public_path('/images/albums/' . $album->slug . '/thumb_' . $photo->url));
        return response(['flash' => 'Removed']);
    }

    public function storeOrder()
    {
        $i = 0;
        foreach (request('data') as $photoId) {
            Photo::find($photoId)->update(['order_id' => $i]);
            $i++;
        }
        return response(['status' => 'Orders updated'], 200);
    }

    public function downloadPhotos(Album $album)
    {
        \File::delete(public_path('test.zip'));
        $files = glob(public_path('images/albums/' . $album->slug));
        $zipper = new \Chumper\Zipper\Zipper;
        $zipper->make(public_path('test.zip'))->add($files)->close();
        return response()->download(public_path('test.zip'), $album->slug . '.zip', ['Content-Type' => 'application/octet-stream']);
    }

    public function videos(Album $album)
    {
        //$photos = $album->photos;
        $album = $album->load('videos');
        return view('admin.albums.videos', compact('album'));
    }

    public function addVideo(Album $album)
    {
        $album->videos()->syncWithoutDetaching(request('video_id'));
        return back()->with('flash', 'Video added to album');
    }

    public function removeVideo(Album $album, $video_id)
    {
        $album->videos()->detach($video_id);
        return back()->with('flash', 'Video removed from album');
    }
}
