@extends('layouts.main')
@section('title','Indochina Unique Tourist')
@section('header')
<style>
    .sub-menu li a:after, div#reviews ~ div span:after {
        content: "";
        height: 3px;
        left: 0;
        width: 15px;
        bottom: -3px;
        position: absolute;
        transition: all .3s ease-in-out;
        background-color: #ffc200;
    }

    .sub-menu li a:hover:after, div#reviews ~ div span:hover:after {
        width: 100%;
    }

    .tooltip .tooltiptext {
  width: auto;
  white-space: nowrap;
  background-color: #ffc200;
  color: #fff;
  text-align: center;
  border-radius: 6px;
  padding: 5px 10px;
  position: absolute;
  z-index: 1;
  bottom: 150%;
  left: 50%;
  margin-left:-80px
}

.tooltip .tooltiptext::after {
  content: "";
  position: absolute;
  top: 100%;
  left: 50%;
  margin-left: -5px;
  border-width: 5px;
  border-style: solid;
  border-color: #ffc200 transparent transparent transparent;
}
.heart-button:hover i {
    transform: scale(1.2) skewX(12deg);
} 
</style>
@endsection
@section('content')
<div class="relative bg-center bg-no-repeat bg-cover"
     style="height:380px;background-image:url('/images/cj2u9oztfb_cover.jpg')">
    <div class="absolute bottom-0 left-0 right-0 text-center text-white"
         style="height:120px;background-image:linear-gradient(rgba(0,0,0,0) 0,rgba(0,0,0,.9) 100%);">
        <p class="text-sm">Da Nang</p>
        <h1 class="text-xl md:text-3xl font-medium tracking-tight text-orangeMain uppercase">La Huong Organic Vegetable Village
            Tour</h1>
        <div class="flex items-center justify-center">
            <div>
                <i class="fas fa-star text-orangeMain"></i>
                <i class="fas fa-star text-orangeMain -ml-1"></i>
                <i class="fas fa-star text-orangeMain -ml-1"></i>
                <i class="far fa-star -ml-1"></i>
                <i class="far fa-star -ml-1"></i>
            </div>
            <p class="text-sm ml-3">1 reviews</p>
        </div>
    </div>
</div>
<div class="sticky my-3 z-20 bg-white shadow-lg pb-3 overflow-x-scroll md:overflow-hidden" style="top:57px;">
    <div class="container mx-auto">
        <ul class="text-gray-700 sub-menu flex">
            <li class="ml-4 relative border-b border-gray-400"><a href="#overview" @click="$scrollTo('#overview',400,{offset:-94})">OVERVIEW</a></li>
            <li class="ml-4 relative border-b border-gray-400"><a href="#gallery" @click="$scrollTo('#itinerary',400,{offset:-94})">ITINERARY</a></li>
            <li class="ml-4 relative border-b border-gray-400"><a href="#information" @click="$scrollTo('#information',400,{offset:-94})">INFORMATION</a></li>
            <li class="ml-4 relative border-b border-gray-400"><a href="#gallery" @click="$scrollTo('#gallery',400,{offset:-94})">GALLERY</a></li>
            <li class="ml-4 relative border-b border-gray-400"><a href="#reviews" @click="$scrollTo('#reviews',400,{offset:-94})">REVIEWS</a></li>
            <li class="ml-4 relative border-b border-gray-400"><a href="#others" @click="$scrollTo('#others',400,{offset:-94})">OTHERS</a></li>
        </ul>
    </div>    
    </div>
<div class="container mx-auto">
    
    <div class="lg:grid lg:grid-cols-12 lg:col-gap-4">
        <div class="lg:col-span-8">
            

            <div id="overview" class="pb-3 px-3">
                <h1 class="text-2xl text-orangeMain">Overview</h1>
                <hr>
                <p class="tracking-tight leading-snug text-gray-700">La Huong is a vegetable traditional village located
                    in the
                    countryside of Da Nang city. Various types of vegetables are cultivated by organic methods in the
                    village.
                    You
                    meet and talk to local farmers, experience as farmers as well as getting your hand busy by making
                    traditional
                    cakes.</p>
            </div>
            <div id="itinerary" class="p-3">
                <h1 class="text-2xl text-orangeMain">Itinerary</h1>
                <hr>
                <p class="tracking-tight leading-snug text-gray-700">La Huong is a vegetable traditional village located
                    in the
                    countryside of Da Nang city. Various types of vegetables are cultivated by organic methods in the
                    village.
                    You
                    meet and talk to local farmers, experience as farmers as well as getting your hand busy by making
                    traditional
                    cakes.</p>
                <p class="tracking-tight leading-snug text-gray-700">La Huong is a vegetable traditional village located
                    in the
                    countryside of Da Nang city. Various types of vegetables are cultivated by organic methods in the
                    village.
                    You
                    meet and talk to local farmers, experience as farmers as well as getting your hand busy by making
                    traditional
                    cakes.</p>
                <p class="tracking-tight leading-snug text-gray-700">La Huong is a vegetable traditional village located
                    in the
                    countryside of Da Nang city. Various types of vegetables are cultivated by organic methods in the
                    village.
                    You
                    meet and talk to local farmers, experience as farmers as well as getting your hand busy by making
                    traditional
                    cakes.</p>
            </div>
            <div id="information" class="p-3 text-gray-700 bg-blue-800 rounded">
                <h1 class="text-2xl text-orangeMain">Important Information</h1>
                <hr>
                <div class="py-2 grid grid-cols-1 md:grid-cols-3 md:col-gap-2">
                    <div>
                        <h3 class="text-xl text-orangeMain">Tour Code</h3>
                        <p class="text-white">DAD03</p>
                        <h3 class="text-xl mt-2 text-orangeMain">Departure Point</h3>
                        <p class="text-white">Your hotel</p>
                        <h3 class="text-xl mt-2 text-orangeMain">Departure Time</h3>
                        <p class="text-white">08:30</p>
                        <h3 class="text-xl mt-2 text-orangeMain">Duration</h3>
                        <p class="text-white">5 hours</p>
                    </div>
                    <div>
                        <h3 class="text-xl text-orangeMain">Inclusions</h3>
                        <ul class="ml-6 list-disc text-white">
                            <li>Hotel pickup and drop-off by van from Da Nang</li>
                            <li>Hotel pickup and drop-off by van from Da Nang</li>
                            <li>Hotel pickup and drop-off by van from Da Nang</li>
                        </ul>
                    </div>
                    <div>
                        <h3 class="text-xl text-orangeMain">Exclusions</h3>
                        <ul class="ml-6 list-disc text-white">
                            <li>Hotel pickup and drop-off by van from Da Nang</li>
                            <li>Hotel pickup and drop-off by van from Da Nang</li>
                            <li>Hotel pickup and drop-off by van from Da Nang</li>
                        </ul>
                    </div>
                </div>
                <div>
                    <h3 class="text-xl text-orangeMain">Additional Info</h3>
                    <hr>
                    <p class="text-white">Transfer from hotel accommodations in the outskirts of Danang city could be subject to charge
                        additional
                        fees.</p>
                </div>
                <div class="pt-2">
                    <h3 class="text-xl text-orangeMain">Cancellation Policy</h3>
                    <hr>
                    <p class="text-white">Free cancellation policy applied for this tour package is 24 hours prior to departure date.</p>
                </div>
            </div>

            <div id="gallery" class="p-3">
                <h1 class="text-2xl text-orangeMain">Gallery</h1>
                <hr class="mb-3">
                <three-at-a-time-slider></three-at-a-time-slider>
            </div>

            <div id="reviews" class="p-3">
                <h1 class="text-2xl text-orangeMain">Reviews</h1>
                <hr class="mb-3">
                <div>
                    <span class="text-white p-1 rounded bg-red-700">On this site (0)</span>
                    <span class="text-green-600 p-1 ml-3 relative">Distribution channels (0)</span>
                </div>
                <h3 class="text-4xl text-orangeMain">0.0</h3>
                <div class="text-gray-700">
                    <i class="fas fa-star text-orangeMain"></i>
                    <i class="fas fa-star text-orangeMain"></i>
                    <i class="fas fa-star text-orangeMain"></i>
                    <i class="far fa-star"></i>
                    <i class="far fa-star"></i>
                </div>
                <p class="mt-2 text-sm text-gray-700">Have you experienced with us on this tour? Please share your reviews with us!</p>
                <hr>
                {{-- <p class="text-gray-700 my-3">There is no review yet for this trip.</p>
                <a href=""
               class="uppercase mt-1 p-1 font-medium inline-block bg-orangeMain rounded text-sm text-white transform -skew-x-12 hover:bg-yellow-600">
                <span class="transform skew-x-12 block ml-2">Be the first to leave review <i
                       class="fas fa-chevron-right ml-2"></i></span></a> --}}
                <div class="my-3">
                    <div class="text-gray-700 relative"><img src="/images/TripAdvisor.svg" alt="" class="absolute right-0 top-0 h-6 w-auto">
                        <div class="flex">
                        <div class="w-12 h-12 p-2 flex-shrink-0 text-xl font-medium rounded-full text-white bg-yellow-600 flex justify-center items-center">D</div>
                        <div class="ml-3">
                            <p class=" font-medium">Dennis</p>
                            <div>
                                <i class="fas fa-star text-orangeMain fa-xs"></i>
                                <i class="fas fa-star text-orangeMain fa-xs -ml-1"></i>
                                <i class="fas fa-star text-orangeMain fa-xs -ml-1"></i>
                                <i class="far fa-star fa-xs -ml-1"></i>
                                <i class="far fa-star fa-xs -ml-1"></i>

                                <span class="text-xs ml-3"><i class="far fa-calendar-alt"></i> <span class="ml-1">5 Months Ago</span></span>
                            </div>
                            <p class="leading-snug">his tour is a must if you are visiting Vietnam! Our guide (Ms. Thach) spoke fluently english and had great knowlege about the War + DMZ + history of Vietnam. We learned a lot and could follow her words easily. The comunication with the Indochina Company (especially Ms. Anh) was also</p>
                        </div>
                        
                    </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="md:w-1/2 md:mx-auto lg:w-full lg:col-span-4 lg:mr-3 lg:sticky  mx-3" style="top:94px; height: fit-content;">
        <div class="rounded border border-blue-400">
            <p class="h-10 bg-blue-700 border-b border-blue-400 text-white font-medium flex items-center justify-center">BOOKING</p>
            <div class="py-3 px-6 flex items-center">
                <span class="text-sm text-gray-700 w-1/3 text-right">Select Date:</span>
                <div class="relative ml-3">
                    <input type="text" readonly placeholder="MM/DD/YYYY" class="py-1 px-2 placeholder-gray-600 text-sm rounded border border-blue-400 w-40 h-10 focus:outline-none">
                    <i class="far fa-calendar-alt absolute" style="top:10px; right: 8px;"></i>
                </div>
            </div>
            <div class="pb-3 px-6 flex items-center ">
                <span class="text-sm text-gray-700 w-1/3 text-right">Guests:</span>
                <div class="relative ml-3">
                    <input type="text" readonly placeholder="2 pax" class="py-1 px-2 placeholder-gray-600 text-sm rounded border border-blue-400 w-40 h-10 focus:outline-none">
                    <i class="fas fa-users-alt absolute" style="top:10px; right: 8px;"></i>
                </div>
            </div>
            <div class="pb-3 px-6 flex items-center justify-center text-gray-700">
                <span>2 </span><img src="/images/icons/adult.png" alt="">
                <span class="ml-3">2 </span><img src="/images/icons/child.png" alt="">
                <span class="ml-3">2 </span><img src="/images/icons/infant.png" alt="">
            </div>
            <div x-data="{openPaxInfo1: false}" class="pb-3 px-6 text-gray-700 flex items-center">
                <span class="text-sm text-gray-700 w-1/3 text-right">Adults <i x-on:mouseenter="openPaxInfo1 = true" x-on:touchstart="openPaxInfo1 = true" x-on:mouseout="openPaxInfo1=false" class="fas fa-info-circle tooltip relative">
                <span x-show="openPaxInfo1" x-on:click.away="openPaxInfo1=false" class="tooltiptext">From 11 to 70 years old</span>    
                </i></span>
                <div class="w-40 h-10 ml-3 rounded border border-blue-400 inline-flex">
                    <span class="w-1/3 h-full border-r border-blue-400 flex justify-center items-center"><i class="fas fa-minus"></i></span>
                    <span class="w-1/3 h-full flex justify-center items-center font-medium">2</span>
                    <span class="w-1/3 h-full border-l border-blue-400 flex justify-center items-center"><i class="fas fa-plus"></i></span>
                </div>
            </div>
            <div x-data="{openPaxInfo2: false}" class="pb-3 px-6 text-gray-700 flex items-center">
                <span class="text-sm text-gray-700 w-1/3 text-right">Children <i x-on:mouseenter="openPaxInfo2 = true" x-on:touchstart="openPaxInfo2 = true" x-on:mouseout="openPaxInfo2=false" class="fas fa-info-circle tooltip relative">
                <span x-show="openPaxInfo2" x-on:click.away="openPaxInfo2=false" class="tooltiptext">From 11 to 70 years old</span>    
                </i></span>
                <div class="w-40 h-10 ml-3 rounded border border-blue-400 inline-flex">
                    <span class="w-1/3 h-full border-r border-blue-400 flex justify-center items-center"><i class="fas fa-minus"></i></span>
                    <span class="w-1/3 h-full flex justify-center items-center font-medium">2</span>
                    <span class="w-1/3 h-full border-l border-blue-400 flex justify-center items-center"><i class="fas fa-plus"></i></span>
                </div>
            </div>
            <div x-data="{openPaxInfo3: false}" class="pb-3 px-6 text-gray-700 flex items-center">
                <span class="text-sm text-gray-700 w-1/3 text-right">Infants <i x-on:mouseenter="openPaxInfo3 = true" x-on:touchstart="openPaxInfo3 = true" x-on:mouseout="openPaxInfo3=false" class="fas fa-info-circle tooltip relative">
                <span x-show="openPaxInfo3" x-on:click.away="openPaxInfo3=false" class="tooltiptext">From 11 to 70 years old</span>    
                </i></span>
                <div class="w-40 h-10 ml-3 rounded border border-blue-400 inline-flex">
                    <span class="w-1/3 h-full border-r border-blue-400 flex justify-center items-center"><i class="fas fa-minus"></i></span>
                    <span class="w-1/3 h-full flex justify-center items-center font-medium">2</span>
                    <span class="w-1/3 h-full border-l border-blue-400 flex justify-center items-center"><i class="fas fa-plus"></i></span>
                </div>
            </div>
            <div class="text-center">
            <a href=""
               class="uppercase mb-3 text-center p-1 font-medium inline-block bg-orangeMain rounded text-sm text-white transform -skew-x-12 hover:bg-yellow-600">
                <span class="transform skew-x-12 block ml-2">Update search <i class="fas fa-search-dollar mx-2"></i></span></a>
            </div>
            
        </div>
        <div class="text-center mt-3">
            <p class="flex items-center justify-center text-gray-700 text-sm">
                Add to favorites list 
                <button class="heart-button rounded transform -skew-x-12 py-1 px-3 bg-orangeMain ml-2"><i class="fas fa-heart text-red-700 transform skew-x-12"></i></button>
            </p>
            <div class="mt-1">
                <share-network
                    network="facebook"
                    url="https://news.vuejs.org/issues/180"
                    title="Say hi to Vite! A brand new, extremely fast development setup for Vue."
                    description="This week, I’d like to introduce you to 'Vite', which means 'Fast'. It’s a brand new development setup created by Evan You."
                >
                    <i class="fab fa-facebook-square fa-lg text-blue-800 cursor-pointer"></i>
                </share-network>
                <share-network
                    network="twitter"
                    url="https://news.vuejs.org/issues/180"
                    title="Say hi to Vite! A brand new, extremely fast development setup for Vue."
                    description="This week, I’d like to introduce you to 'Vite', which means 'Fast'. It’s a brand new development setup created by Evan You."
                >
                <i class="fab fa-twitter-square fa-lg text-blue-500 cursor-pointer"></i>
                </share-network>
                <share-network
                    network="line"
                    url="https://news.vuejs.org/issues/180"
                    title="Say hi to Vite! A brand new, extremely fast development setup for Vue."
                    description="This week, I’d like to introduce you to 'Vite', which means 'Fast'. It’s a brand new development setup created by Evan You."
                >
                <i class="fab fa-line fa-lg text-green-600 cursor-pointer"></i>
                </share-network>
                <share-network
                    network="email"
                    url="https://news.vuejs.org/issues/180"
                    title="Say hi to Vite! A brand new, extremely fast development setup for Vue."
                    description="This week, I’d like to introduce you to 'Vite', which means 'Fast'. It’s a brand new development setup created by Evan You."
                >
                <i class="fas fa-envelope-square fa-lg text-yellow-700 cursor-pointer"></i>
                </share-network>
            </div>
        </div>
    </div>
</div>

    <div id="others" class="p-3">
        <h1 class="text-2xl text-orangeMain">You May Also Like </h1>
        <hr>
        <similar-tours></similar-tours>
    </div>
</div>
@endsection

@section('footer')
{{-- <script src="js/homepage_combined.js?v=3"></script> --}}
@endsection