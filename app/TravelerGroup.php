<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TravelerGroup extends Model
{
    public $timestamps = false;
    protected $guarded = [];

    protected static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            $model->slug = str_slug($model->name);
        });
    }
}
