<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
    public $timestamps = false;
    protected $guarded = [];
    protected $with = ['travelerGroup'];

    public function travelerGroup()
    {
        return $this->belongsTo(TravelerGroup::class);
    }
}
